<?php
session_start();
require_once 'functions.php';
Authorization();
include_once 'config.php';
include_once 'loan-function.php';
include "header.php";
$action = $_POST['action'] ?? '';
$task = $_GET['task'] ?? '';
$tatus = 0;

if ('addRemember' == $action) {
    $person_id = filter_input(INPUT_POST, 'person_id', FILTER_SANITIZE_STRING);
    $receiving_date = filter_input(INPUT_POST, 'receiving_date', FILTER_SANITIZE_STRING);
    $deposit_date = filter_input(INPUT_POST, 'deposit_date', FILTER_SANITIZE_STRING);
    $check_number = filter_input(INPUT_POST, 'check_number', FILTER_SANITIZE_STRING);
    $amount = filter_input(INPUT_POST, 'amount', FILTER_SANITIZE_STRING);

    addRemember($person_id,$receiving_date,$deposit_date,$check_number,$amount);

}
if ('updateRemember' == $action) {
    $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
    $person_id = filter_input(INPUT_POST, 'person_id', FILTER_SANITIZE_STRING);
    $receiving_date = filter_input(INPUT_POST, 'receiving_date', FILTER_SANITIZE_STRING);
    $deposit_date = filter_input(INPUT_POST, 'deposit_date', FILTER_SANITIZE_STRING);
    $check_number = filter_input(INPUT_POST, 'check_number', FILTER_SANITIZE_STRING);
    $amount = filter_input(INPUT_POST, 'amount', FILTER_SANITIZE_STRING);
    updateRemember($id,$person_id,$receiving_date,$deposit_date,$check_number,$amount);
}

if ('delete' == $task) {
    $id = $_GET['id'];
    deleteRembember($id);
}
?>

<!-- Start Content -->
<div class="layout-px-spacing">
    <!-- Start breadcrumb -->
    <div class="page-header">
        <div class="page-title">
            <h3>নতুন স্মরণ যোগ করুন</h3>
        </div>
        <nav class="breadcrumb-one" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-home">
                            <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                            <polyline points="9 22 9 12 15 12 15 22"></polyline>
                        </svg>
                    </a></li>
                <li class="breadcrumb-item active" aria-current="page"><span>স্মরণ </span></li>
            </ol>
        </nav>
    </div>
    <!-- End breadcrumb -->
    <!-- CONTENT AREA -->
    <?php
    $status = $_GET['status'] ?? 0;
    if (20 == $status) { ?>
        <div class="row">
            <div class="col-8 offset-sm-4">
                <div class="alert alert-info mb-4" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-x close" data-dismiss="alert">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                    <strong>Well Done !!</strong> <?php echo getStatusMessage($status); ?></button>
                </div>
            </div>
        </div>
    <?php } elseif(21 == $status){ ?>
        <div class="row">
            <div class="col-8 offset-sm-4">
                <div class="alert alert-warning mb-4" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-x close" data-dismiss="alert">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                    <strong>Warning !!</strong> <?php echo getStatusMessage($status); ?></button>
                </div>
            </div>
        </div>
    <?php }elseif (22 == $status){ ?>
        <div class="row">
            <div class="col-8 offset-sm-4">
                <div class="alert alert-success mb-4" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-x close" data-dismiss="alert">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                    <strong>Well Done !!</strong> <?php echo getStatusMessage($status); ?></button>
                </div>
            </div>
        </div>
    <?php }elseif (23 == $status){ ?>
        <div class="row">
            <div class="col-8 offset-sm-4">
                <div class="alert alert-success mb-4" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-x close" data-dismiss="alert">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                    <strong>Well Done !!</strong> <?php echo getStatusMessage($status); ?></button>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="row layout-top-spacing">
        <div class="col-4 layout-spacing">
            <div class="widget-content-area br-4">
                <div class="widget-one">
                    <h5 class="text-center">নতুন স্মরণ </h5>
                    <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>"
                          enctype="multipart/form-data">
                        <div class="form-group mb-2">
                            <label for="person_name">ব্যক্তি নাম</label>
                            <select name="person_id" id="person_id" class="form-control">
                                <option value="" selected>Select Person</option>
                                <?php
                                $person = getAllPersong();
                                while ($data = mysqli_fetch_assoc($person)) {
                                    ?>
                                    <option
                                        value="<?php echo $data['id']; ?>"><?php echo $data['person_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group mb-2">
                            <label for="receiving_date">জমার তারিখ</label>
                            <input type="date" name="receiving_date" class="form-control">
                        </div>
                        <div class="form-group mb-2">
                            <label for="deposit_date">উইথড্র তারিখ</label>
                            <input type="date" name="deposit_date" class="form-control">
                        </div>
                        <div class="form-group mb-2">
                            <label for="check_number">চেক নাম্বার </label>
                            <input type="text" name="check_number" class="form-control">
                        </div>
                        <div class="form-group mb-2">
                            <label for="amount">টাকার পরিমান  </label>
                            <input type="text" name="amount" class="form-control">
                        </div>
                        <input type="submit" name="submit" value="সাবমিট" class="btn btn-primary btn-block mb-4 mr-2">
                        <input type="hidden" name="action" id="action" value="addRemember">
                    </form>

                </div>
            </div>
        </div>
        <div class="col-8 layout-spacing">
            <div class="widget-content-area br-4">
                <div class="widget-one">
                    <h5 class="text-center">স্মরণ তালিকা</h5>
                    <div class="table-responsive mb-4">
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                            <tr>
                                <td>sl</td>
                                <td> ব্যক্তি নাম</td>
                                <td>ফোন নাম্বার </td>
                                <td>জমার তারিখ</td>
                                <td>উইথড্র তারিখ</td>
                                <td>চেক নাম্বার</td>
                                <td>টাকার পরিমান</td>
                                <td>Action</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $data = getRemember();
                            $count = 1;
                            while ($rememberInfo = mysqli_fetch_assoc($data)){
                            ?>
                            <tr>
                                <td><?php echo $count; ?></td>
                                <td><?php echo $rememberInfo['person_name']; ?></td>
                                <td><?php echo $rememberInfo['person_phone_no']; ?></td>
                                <td><?php echo $rememberInfo['receiving_date']; ?></td>
                                <td><?php echo $rememberInfo['deposit_date']; ?></td>
                                <td><?php echo $rememberInfo['check_number']; ?></td>
                                <td><?php echo $rememberInfo['amount']; ?></td>
                                <td>
                                    <?php printf("<a class='delete' href='remember-list.php?task=delete&id=%s' onclick='return confirmDelete()'><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-x-circle table-cancel\"><circle cx=\"12\" cy=\"12\" r=\"10\"></circle><line x1=\"15\" y1=\"9\" x2=\"9\" y2=\"15\"></line><line x1=\"9\" y1=\"9\" x2=\"15\" y2=\"15\"></line></svg></a>", $rememberInfo['id']) ?>
                                    <a type="button" class="" data-toggle="modal"
                                       data-target="#updateRemember<?php echo $rememberInfo['id']; ?>">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                             stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-edit-3">
                                            <path d="M12 20h9"></path>
                                            <path
                                                d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path>
                                        </svg>
                                    </a>
                                </td>
                            </tr>
                            <div class="modal fade" id="updateRemember<?php echo $rememberInfo['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">স্মরণ পরিবর্তন </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="widget-one">
                                                <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                                                    <div class="form-group mb-2">
                                                        <label for="billDesc">ব্যক্তি নাম </label>
                                                        <select name="person_id" id="person_id" class="form-control">
                                                            <?php
                                                            $person_name = getAllPersong();
                                                            while ($personInfo = mysqli_fetch_assoc($person_name)){ ?>
                                                                <option value="<?php echo $personInfo['id'];?>" <?php if($personInfo['id'] == $rememberInfo['person_id']){ echo 'selected';}?>><?php echo $personInfo['person_name'];?></option>
                                                            <?php } ?>
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group mb-2">
                                                        <label for="receiving_date">জমার তারিখ</label>
                                                        <input type="date" name="receiving_date" class="form-control" value="<?php echo $rememberInfo['receiving_date']; ?>">
                                                    </div>
                                                    <div class="form-group mb-2">
                                                        <label for="deposit_date">উইথড্র তারিখ</label>
                                                        <input type="date" name="deposit_date" class="form-control" value="<?php echo $rememberInfo['deposit_date']; ?>">
                                                    </div>
                                                    <div class="form-group mb-2">
                                                        <label for="check_number">চেক নাম্বার </label>
                                                        <input type="text" name="check_number" class="form-control" value="<?php echo $rememberInfo['check_number']; ?>">
                                                    </div>
                                                    <div class="form-group mb-2">
                                                        <label for="amount">টাকার পরিমান  </label>
                                                        <input type="text" name="amount" class="form-control" value="<?php echo $rememberInfo['amount']; ?>">
                                                    </div>
                                                    <input type="submit" name="submit" value="সাবমিট" class="btn btn-primary btn-block mb-4 mr-2">
                                                    <input type="hidden" name="id"  value="<?php echo $rememberInfo['id']; ?>">
                                                    <input type="hidden" name="action" id="action" value="updateRemember">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <?php $count++;
                } ?>
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- End Content -->
<?php include "footer.php"; ?>

<script>
    function confirmDelete() {
        if (confirm("Are you sure want to delete?")) {
            return true;
        }
        return false;
    }

    $(document).ready(function(){
        var maxField = 5;
        var addButton = $('.add_button');
        var wrapper = $('.field_wrapper');
        var fieldHTML = '<div>' +
            '<a href="javascript:void(0);" class="remove_button btn btn-danger mt-2 mb-4  btn-sm">' +
            '<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><line x1="8" y1="12" x2="16" y2="12"></line></svg>' +
            '</a>' +
            '<input type="text" name="billDesc[]" placeholder="Description here" class="form-control"/>' +' <input type="text" class="form-control mt-2" name="billAmount[]" placeholder="0.00"  required/>'+
            '</div>';
        var x = 1;

        $(addButton).click(function(){
            if(x < maxField){
                x++;
                $(wrapper).append(fieldHTML);
            }
        });

        $(wrapper).on('click', '.remove_button', function(e){
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
        });
    });
</script>
