<?php
session_start();
require_once 'functions.php';
Authorization();
include_once 'config.php';
include "header.php";
$action  = $_POST['action'] ?? '';
$task = $_GET['task'] ?? '';
$status = 0;

if('addExpense' == $action){
    $expense_category_id = filter_input(INPUT_POST,'expense_category_id', FILTER_SANITIZE_STRING);
    $expense_amount = filter_input(INPUT_POST,'expense_amount', FILTER_SANITIZE_STRING);
    $expense_date = filter_input(INPUT_POST,'expense_date', FILTER_SANITIZE_STRING);
    $expense_details = filter_input(INPUT_POST,'expense_details', FILTER_SANITIZE_STRING);
    addExpense($expense_category_id,$expense_amount,$expense_date,$expense_details);
}
if ('updateCost' == $action) {
    $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
    $expense_category_id = filter_input(INPUT_POST,'expense_category_id', FILTER_SANITIZE_STRING);
    $expense_amount = filter_input(INPUT_POST, 'expense_amount', FILTER_SANITIZE_STRING);
    $expense_date = filter_input(INPUT_POST, 'expense_date', FILTER_SANITIZE_STRING);
    $expense_details = filter_input(INPUT_POST, 'expense_details', FILTER_SANITIZE_STRING);

    updateExpenseInfo($id, $expense_category_id, $expense_amount, $expense_date,$expense_details);
}

if('delete' == $task){
    $id = $_GET['id'];
    deleteExpense($id);
}
?>
<!-- Start Content -->
<div class="layout-px-spacing">
    <!-- Start breadcrumb -->
    <div class="page-header">
        <div class="page-title">
            <h3>খরচের তথ্যসমূহ</h3>
        </div>
        <nav class="breadcrumb-one" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-home">
                            <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                            <polyline points="9 22 9 12 15 12 15 22"></polyline>
                        </svg>
                    </a></li>
                <li class="breadcrumb-item active" aria-current="page"><span>খরচ</span></li>
            </ol>
        </nav>
    </div>
    <!-- End breadcrumb -->
    <!-- CONTENT AREA -->
    <?php
    $status = $_GET['status'] ?? 0;
    if (13 == $status) { ?>
        <div class="row">
            <div class="col-8 offset-sm-4">
                <div class="alert alert-info mb-4" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-x close" data-dismiss="alert">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                    <strong>Well Done !!</strong> <?php echo getStatusMessage($status); ?></button>
                </div>
            </div>
        </div>
    <?php } elseif (14 == $status) { ?>
        <div class="row">
            <div class="col-8 offset-sm-4">
                <div class="alert alert-warning mb-4" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-x close" data-dismiss="alert">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                    <strong>Warning !! </strong> <?php echo getStatusMessage($status); ?></button>
                </div>
            </div>
        </div>
    <?php } elseif (15 == $status) { ?>
        <div class="row">
            <div class="col-8 offset-sm-4">
                <div class="alert alert-success mb-4" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-x close" data-dismiss="alert">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                    <strong>Success !! </strong> <?php echo getStatusMessage($status); ?></button>
                </div>
            </div>
        </div>
   <?php } elseif (32 == $status) { ?>
    <div class="row">
        <div class="col-8 offset-sm-4">
            <div class="alert alert-success mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-x close" data-dismiss="alert">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                <strong>Well Done !! </strong> <?php echo getStatusMessage($status); ?></button>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="row layout-top-spacing">
        <div class="col-4 layout-spacing">
            <div class="widget-content-area br-4">
                <div class="widget-one">
                    <h5 class="text-center">নতুন খরচের তথ্য</h5>
                    <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                        <div class="form-group mb-4">
                            <label for="expense_category_id">খরচের ক্যাটাগরি</label><br>
                            <select class="form-control selectpicker" name="expense_category_id" data-live-search="true">
                                <option value="">Select Category</option>
                                <?php
                                $result = getAllCategory();
                                $count = 1;
                                while ($rows = mysqli_fetch_assoc($result)) {
                                    if (5 == $rows['id']) {
                                        ?>
                                        <option value="<?php echo $rows['id']; ?>"
                                                disabled=""><?php echo $rows['category_name']; ?></option>
                                    <?php } else { ?>
                                        <option
                                            value="<?php echo $rows['id']; ?>"><?php echo $rows['category_name']; ?></option>
                                    <?php }
                                } ?>

                            </select>
                        </div>
                        <div class="form-group mb-4">
                            <label for="takatotla">টাকার পরিমান </label>
                            <input id="takatotla" type="number" name="expense_amount" placeholder="২০,২৫০ টাকা "
                                   class="form-control" required="">
                        </div>
                        <div class="form-group mb-4">
                            <label for="dateb">তারিখ </label>
                            <input id="basicFlatpickr" name="expense_date"
                                   class="form-control flatpickr flatpickr-input active" type="text"
                                   placeholder="Select Date.." readonly="readonly">
                        </div>
                        <div class="form-group mb-4">
                            <label for="Detailsb">বিস্তারিত </label>
                            <textarea class="form-control" id="Detailsb" name="expense_details" rows="2"></textarea>
                        </div>
                        <input type="submit" name="submit" value="সাবমিট" class="btn btn-primary btn-block mb-4 mr-2">
                        <input type="hidden" name="action" id="action" value="addExpense">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-8 layout-spacing">
            <div class="widget-content-area br-4">
                <div class="widget-one">
                    <h5 class="text-center">খরচের ক্যাটাগরি তালিকা</h5>
                    <div class="table-responsive mb-4">
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th>সিরিয়াল</th>
                                <th>খরচের ক্যাটাগরি</th>
                                <th>তারিখ</th>
                                <th> টাকার পরিমান</th>
                                <th>বিস্তারিত</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $result = getAllExpense();
                            $count = 1;
                            while ($rows = mysqli_fetch_assoc($result)) { ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><?php echo $rows['category_name']; ?></td>
                                    <td><?php echo $rows['expense_date']; ?></td>
                                    <td><?php echo bn(number_format($rows['expense_amount'])); ?></td>
                                    <td><?php echo $rows['expense_details']; ?></td>
                                    <td>
                                        <?php if (5 == $rows['expense_category_id']) { ?>
                                            <?php printf("<a class='delete' href='javascript:void(0);'><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-x-circle table-cancel\"><circle cx=\"12\" cy=\"12\" r=\"10\"></circle><line x1=\"15\" y1=\"9\" x2=\"9\" y2=\"15\"></line><line x1=\"9\" y1=\"9\" x2=\"15\" y2=\"15\"></line></svg></a>") ?>
                                        <?php } else { ?>
                                            <?php printf("<a class='delete' href='AddNewCosts.php?task=delete&id=%s' onclick='return confirmDelete()'><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-x-circle table-cancel\"><circle cx=\"12\" cy=\"12\" r=\"10\"></circle><line x1=\"15\" y1=\"9\" x2=\"9\" y2=\"15\"></line><line x1=\"9\" y1=\"9\" x2=\"15\" y2=\"15\"></line></svg></a>", $rows['id']) ?>
                                            <a type="button" class="" data-toggle="modal" data-target="#costUpdate<?php echo $rows['id'];?>">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                     stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                     class="feather feather-edit-3">
                                                    <path d="M12 20h9"></path>
                                                    <path
                                                            d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path>
                                                </svg>
                                            </a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <div class="modal fade" id="costUpdate<?php echo $rows['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalCenterTitle">খরচের তথ্য</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="widget-one">
                                                    <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                                                        <div class="form-group mb-4">
                                                            <label for="expense_category">খরচের ক্যাটাগরি</label><br>
                                                            <!--<input type="text" name="expense_category_id" value="<?php /*echo $rows['expense_category_id']; */?>">-->
                                                            <select name="expense_category_id" id="expense_category_id" class="form-control">
                                                                <?php
                                                                $catResult = catlist();
                                                                while ($category = mysqli_fetch_assoc($catResult)){
                                                                    ?>
                                                                    <option value="<?php echo $category['id'];?>" <?php if($category['id'] == $rows['expense_category_id']){ echo 'selected';}?>><?php echo $category['category_name'];?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <label for="takatotla">টাকার পরিমান </label>
                                                            <input id="takatotla" type="number" name="expense_amount" value="<?php echo $rows['expense_amount']; ?>"
                                                                   class="form-control" required="">
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <label for="dateb">তারিখ </label>
                                                            <input type="date" id="date" name="expense_date"
                                                                   class="form-control flatpickr flatpickr-input active" type="text"
                                                                   placeholder="Select Date.." value="<?php echo $rows['expense_date']; ?>">
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <label for="Detailsb">বিস্তারিত </label>
                                                            <textarea class="form-control" id="Detailsb" name="expense_details" rows="2"><?php echo $rows['expense_details']; ?></textarea>
                                                        </div>
                                                        <input type="submit" name="submit" value="সাবমিট" class="btn btn-primary btn-block mb-4 mr-2">
                                                        <input type="hidden" name="id"  value="<?php echo $rows['id']; ?>">
                                                        <input type="hidden" name="action" id="action" value="updateCost">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $count++;
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
			<!-- End Content -->

<?php include "footer.php"; ?>
<script>
    function confirmDelete() {
        if (confirm("Are you sure want to delete?")) {
            return true;
        }
        return false;
    }
</script>
