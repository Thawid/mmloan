<?php
session_start();
include_once 'config.php';
require_once 'functions.php';
Authorization();
include_once 'loan-function.php';
include "header.php";
?>

<!-- Start Content -->
<div class="layout-px-spacing">
    <!-- Start breadcrumb -->
    <div class="page-header">
        <div class="page-title">
            <h3>বকেয়া তালিকা</h3>
        </div>
        <nav class="breadcrumb-one" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-home">
                            <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                            <polyline points="9 22 9 12 15 12 15 22"></polyline>
                        </svg>
                    </a></li>
                <li class="breadcrumb-item active" aria-current="page"><span> বকেয়া তালিকা </span></li>
            </ol>
        </nav>
    </div>
    <!-- End breadcrumb -->
    <!-- CONTENT AREA -->
    <div class="row layout-top-spacing">
        <div class="col-12 layout-spacing">
            <div class="widget-content-area br-4">
                <div class="widget-one">
                    <!--<h5 class="text-center">বকেয়া তালিকা</h5>-->
                    <div class="table-responsive mb-4">
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                            <tr>
                                <td>নং </td>
                                <td>ব্যক্তির  নাম </td>
                                <td>মোবাইল নাম্বার </td>
                                <td>মোট বিল</td>
                                <td>মোট জমা</td>
                                <td>বকেয়া  </td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $data = getDueList();
                            $count = 1;
                            while ($billInfo = mysqli_fetch_assoc($data)) {
                            ?>
                            <tr>
                                <td><?php echo bn($count); ?></td>
                                <td><?php echo $billInfo['person_type'].' '.$billInfo['person_name']; ?></td>
                                <td><?php echo $billInfo['person_phone_no']; ?></td>
                                <td><?php echo bn(number_format($billInfo['TotalBill'])); ?></td>
                                <td><?php echo bn(number_format($billInfo['TotalCredit'])); ?></td>
                                <td><?php echo bn(number_format($billInfo['TotalBill'] - $billInfo['TotalCredit'])); ?></td>

                            </tr>
                            <div class="modal fade" id="updateBill<?php echo $billInfo['id']; ?>" tabindex="-1"
                                 role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">জমার তথ্য
                                                পরিবর্তন </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24"
                                                     height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                     stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                     class="feather feather-x">
                                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="widget-one">
                                                <form method="post"
                                                      action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                                                    <div class="form-group mb-4">
                                                        <label for="billDesc">ব্যক্তি নাম </label>
                                                        <select name="person_id" id="person_id" class="form-control">
                                                            <?php
                                                            $person_name = getAllPersong();
                                                            while ($personInfo = mysqli_fetch_assoc($person_name)) { ?>
                                                                <option value="<?php echo $personInfo['id']; ?>" <?php if ($personInfo['id'] == $billInfo['person_id']) {
                                                                    echo 'selected';
                                                                } ?>><?php echo $personInfo['person_name']; ?></option>
                                                            <?php } ?>
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group mb-4">
                                                        <label for="credit_date">তারিক </label>
                                                        <input id="credit_date" type="date" name="credit_date"
                                                               value="<?php echo $billInfo['credit_date']; ?>"
                                                               class="form-control" required="">
                                                    </div>
                                                    <div class="form-group mb-4">
                                                        <label for="credit_date">টাকার পরিমান </label>
                                                        <input id="credit_amount" type="text" name="credit_amount"
                                                               value="<?php echo $billInfo['credit_amount']; ?>"
                                                               class="form-control" required="">
                                                    </div>
                                                    <div class="form-group mb-4">
                                                        <label for="reference">রেফারেন্সেস </label>
                                                        <input id="reference" type="text" name="reference"
                                                               value="<?php echo $billInfo['reference']; ?>"
                                                               class="form-control" required="">
                                                    </div>
                                                    <input type="submit" name="submit" value="সাবমিট"
                                                           class="btn btn-primary btn-block mb-4 mr-2">
                                                    <input type="hidden" name="id"
                                                           value="<?php echo $billInfo['id']; ?>">
                                                    <input type="hidden" name="action" id="action" value="updateBill">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <?php $count++;
                } ?>
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- End Content -->
<?php include "footer.php"; ?>

<script>
    function confirmDelete() {
        if (confirm("Are you sure want to delete?")) {
            return true;
        }
        return false;
    }

    $(document).ready(function(){
        var maxField = 5;
        var addButton = $('.add_button');
        var wrapper = $('.field_wrapper');
        var fieldHTML = '<div>' +
            '<a href="javascript:void(0);" class="remove_button btn btn-danger mt-2 mb-4  btn-sm">' +
            '<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><line x1="8" y1="12" x2="16" y2="12"></line></svg>' +
            '</a>' +
            '<input type="text" name="billDesc[]" placeholder="Description here" class="form-control"/>' +' <input type="text" class="form-control mt-2" name="billAmount[]" placeholder="0.00"  required/>'+
            '</div>';
        var x = 1;

        $(addButton).click(function(){
            if(x < maxField){
                x++;
                $(wrapper).append(fieldHTML);
            }
        });

        $(wrapper).on('click', '.remove_button', function(e){
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
        });
    });
</script>
