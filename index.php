<?php
session_start();
require_once 'functions.php';
require_once 'loan-function.php';
Authorization();
include_once 'config.php';
include "header.php";
$connection = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
mysqli_set_charset($connection, "utf8");
if (!$connection) {
    throw new Exception("Cannot connect to database");
}

?>
			<!-- Start Content -->
			<div class="layout-px-spacing">
				<!-- Start breadcrumb -->
				<div class="page-header">
					<div class="page-title">
						<h3>ড্যাশবোর্ড</h3>
					</div>
					<nav class="breadcrumb-one" aria-label="breadcrumb">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="javascript:void(0);"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>

							<li class="breadcrumb-item active" aria-current="page"><span>ড্যাশবোর্ড</span></li>
						</ol>
					</nav>
				</div>
				<!-- End breadcrumb -->
                <!-- CONTENT AREA -->


                <div class="row layout-top-spacing">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-6 layout-spacing">
                        <div class="widget-content-area br-4">
                            <div class="widget-one">
                                <div id="tableSimple" class="col-lg-12 col-12 layout-spacing">
                                    <div class="statbox widget box box-shadow">
                                        <div class="widget-header">
                                            <div class="row">
                                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                    <h4>স্মরণ </h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-content widget-content-area">
                                            <div class="table-responsive">
                                                <table class="table table-bordered mb-4">
                                                    <thead>
                                                    <tr>
                                                        <th> ব্যক্তি নাম</th>
                                                        <th>ফোন নাম্বার </th>
                                                        <th>জমার তারিখ</th>
                                                        <th>উইথড্র তারিখ</th>
                                                        <th>চেক নাম্বার</th>
                                                        <th>টাকার পরিমান</th>

                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                        $todayRemember = todayRemember();
                                                        while ($remember = mysqli_fetch_assoc($todayRemember)){
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $remember['person_name']; ?></td>
                                                        <td><?php echo $remember['person_phone_no']; ?></td>
                                                        <td><?php echo $remember['receiving_date']; ?></td>
                                                        <td><span class="text-success"><?php echo $remember['deposit_date']; ?></span></td>
                                                        <td><?php echo $remember['check_number']; ?></td>
                                                        <td><?php echo $remember['amount']; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-6 layout-spacing">
                        <div class="widget-content-area br-4">
                            <div class="widget-one">

                                <div class="row">
                                    <div class="col-lg-6">
                                        <?php
                                        $totdayBill = todayBill();
                                        $bill = mysqli_fetch_assoc($totdayBill);

                                        ?>
                                        <button class="btn btn-primary mb-4  btn-block btn-lg">
                                            <b> আজকের অদ্যরোজ   </b> </br> <?php echo bn(number_format((float)$bill['amount'])); ?> /=
                                        </button>
                                    </div>
                                    <div class="col-lg-6">
                                        <?php
                                        $todayExpense = todayCredit();
                                        $credit = mysqli_fetch_assoc($todayExpense);
                                        ?>
                                        <button class="btn btn-success mb-4  btn-block btn-lg">
                                            <b> আজকের বাকি আদায়  </b> </br> <?php echo bn(number_format((float)$credit['amount'])); ?> /=
                                        </button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <?php
                                        $monthlyBill = monthlyBill();
                                        $monthly_bill = mysqli_fetch_assoc($monthlyBill);

                                        ?>
                                        <button class="btn btn-primary mb-4  btn-block btn-lg">
                                            <b> মাসের  অদ্যরোজ   </b> </br> <?php echo bn(number_format((float)$monthly_bill['amount'])); ?> /=
                                        </button>
                                    </div>
                                    <div class="col-lg-6">
                                        <?php
                                        $monthlyCredit = monthlyCredit();
                                        $monthly_credit = mysqli_fetch_assoc($monthlyCredit);
                                        ?>
                                        <button class="btn btn-success mb-4  btn-block btn-lg">
                                            <b> মাসের  বাকি আদায়  </b> </br> <?php echo bn(number_format((float)$monthly_credit['amount'])); ?> /=
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<!-- End Content -->
<?php include "footer.php"; ?>
          