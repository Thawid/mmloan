<?php
error_reporting(0);
date_default_timezone_set('Asia/Dhaka');
include_once "config.php";
global $connection;
$connection = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
mysqli_set_charset($connection, "utf8");
if (!$connection) {
    echo mysqli_error($connection);
    throw new Exception("Cannot connect to database");
}


function Authorization() {
    $_user_id = $_SESSION['id'] ?? 0;
    if (!$_user_id) {
        echo("<script>location.href = 'auth.php';</script>");
        die();
    }
}

function bn($str) {
    $search=array("0","1","2","3","4","5",'6',"7","8","9");
    $replace=array("০",'১','২','৩','৪','৫','৬','৭','৮','৯');
    return str_replace($search,$replace,$str);
}

function isLogin($user_email, $password) {
    global $connection;
    if ($user_email && $password) {
        $query = "SELECT id, password, user_type  FROM admin WHERE user_email='{$user_email}'";
        $result = mysqli_query($connection, $query);
        if (mysqli_num_rows($result) > 0) {
            $data = mysqli_fetch_assoc($result);
            $_password = $data['password'];
            $_id = $data['id'];
            if (password_verify($password, $_password)) {
                $_SESSION['id'] = $_id;
                $_SESSION['login'] = true;
                $_SESSION['user_type'] = $data['user_type'];
                echo("<script>location.href = 'index.php';</script>");
                die();
            } else {
                $status = 4;
            }
        } else {
            $status = 5;
        }

    } else {
        $status = 2;
    }
    echo("<script>location.href = 'auth.php?status={$status}';</script>");
}

function changePassword($current_password,$password){
    global $connection;
    if (count($_POST) > 0) {
        $query = "SELECT id, password, user_type  FROM admin WHERE user_type='" . $_SESSION["user_type"] . "'";
        $result = mysqli_query($connection, $query);

        if (mysqli_num_rows($result) > 0) {
            $data = mysqli_fetch_assoc($result);
            $_password = $data['password'];
            $current_password = $_POST["currentPassword"];
            if (password_verify($current_password, $_password)) {
                $password = $_POST["newPassword"];
                $hash = password_hash($password, PASSWORD_BCRYPT);
                mysqli_query($connection, "UPDATE admin set password='" . $hash . "' WHERE user_type='" . $_SESSION["user_type"] . "'");
                $status = 23;
            } else
                $status = 24;
        }
    }
    echo("<script>location.href = 'ResetPassword.php?status={$status}';</script>");
}

function getAdminInfo(){
    global $connection;
    $query = "SELECT user_name,user_email, birth, phone_no, profile_pic, user_type  FROM admin WHERE user_type='" . $_SESSION["user_type"] . "'";
    $result = mysqli_query($connection, $query);
    return $result;
}

function updateAdminProfile($user_name,$user_email,$birth,$phone_no) {
    global $connection;
    if ($user_name != '' && $user_email != '') {
        $allowType = array('image/png', 'image/jpg', 'image/jpeg');
        if (isset($_FILES['profile_pic'])) {
            if (in_array($_FILES['profile_pic']['type'], $allowType) !== false) {
                $profile_pic = $_FILES['profile_pic']['name'];
                move_uploaded_file($_FILES['profile_pic']['tmp_name'], "files/" . $_FILES['profile_pic']['name']);
                $updateInfo = "UPDATE admin SET user_name='{$user_name}', user_email='{$user_email}', birth='{$birth}', phone_no='{$phone_no}',profile_pic='{$profile_pic}' WHERE  user_type = '" . $_SESSION["user_type"] . "'";
                $employeeQuery = mysqli_query($connection, $updateInfo);
            } else {
                $updateInfo = "UPDATE admin SET user_name='{$user_name}', user_email='{$user_email}', birth='{$birth}', phone_no='{$phone_no}' WHERE  user_type = '" . $_SESSION["user_type"] . "'";
                $employeeQuery = mysqli_query($connection, $updateInfo);
            }
            if ($employeeQuery) {
                $status = 25;
            }
        } else {
            $status = 26;
        }

    }else{
        $status = 27;
    }
    echo("<script>location.href = 'AdminUserProfile.php?status={$status}';</script>");
}

function addSale($sale_amount,$sale_date,$sale_details) {
    global $connection;
    if ($sale_amount != '' && $sale_date != '') {
        $query = "INSERT INTO  fishselling (sale_amount,sale_date,sale_details) VALUES('{$sale_amount}','{$sale_date}','{$sale_details}')";
        $result = mysqli_query($connection, $query);
        $selectId ="SELECT id FROM fishselling ORDER BY id DESC limit 1";
        $idQuery = mysqli_query($connection,$selectId);
        $getId = mysqli_fetch_assoc($idQuery);
        $selId = $getId['id'];
        $insertReport = "INSERT INTO accounts (sale_id,se_date,sale_amount) VALUES ('{$selId}','{$sale_date}','{$sale_amount}')";
        $reportQuery = mysqli_query($connection,$insertReport);
        if ($result && $reportQuery) {
            $status = 6;
        }
    } else {
        $status = 7;
    }
    echo("<script>location.href = 'TodaySale.php?status={$status}';</script>");
}

function getAllSale() {
    global $connection;
    $query = "SELECT * FROM fishselling ORDER BY id DESC";
    $result = mysqli_query($connection, $query);
    return $result;

}

function deleteSale($id) {
    global $connection;
    $query = "DELETE fishselling,accounts FROM fishselling INNER JOIN accounts ON fishselling.id = accounts.sale_id WHERE fishselling.id = '{$id}'";
    $result = mysqli_query($connection,$query);
    if($result){
        $status = 8;
    }
    echo("<script>location.href = 'TodaySale.php?status={$status}';</script>");
}

function addCategory($category_name,$category_details){
    global $connection;
    if ($category_name != '') {
        $sql = "SELECT * FROM  expansecategory WHERE category_name = '{$category_name}'";
        $query = mysqli_query($connection,$sql);
        $row = mysqli_num_rows($query);
        if($row<=0){
            $query = "INSERT INTO  expansecategory (category_name,category_details) VALUES('{$category_name}','{$category_details}')";
            $result = mysqli_query($connection, $query);
            if ($result) {
                $status = 9;
            }
        }else{
            $status = 10;
        }
    } else {
        $status = 11;
    }
    echo("<script>location.href = 'CostCategory.php?status={$status}';</script>");
}

function getAllCategory() {
    global $connection;
    $query = "SELECT * FROM expansecategory ORDER BY id DESC";
    $result = mysqli_query($connection, $query);
    return $result;

}
function catlist(){
    global $connection;
    $query = "SELECT id,category_name FROM expansecategory WHERE id != 21";
    $catResult = mysqli_query($connection, $query);
    return $catResult;
}

function deleteCategory($id){
    global $connection;
    $query = "DELETE FROM expansecategory WHERE id = '{$id}' LIMIT 1";
    $result = mysqli_query($connection,$query);
    if($result){
        $status = 12;
    }
    echo("<script>location.href = 'CostCategory.php?status={$status}';</script>");
}

function addExpense($expense_category_id,$expense_amount,$expense_date,$expense_details){
    global $connection;
    if ($expense_category_id != '' && $expense_amount != '') {
        $query = "INSERT INTO  expense (expense_category_id,expense_amount,expense_date,expense_details) VALUES('{$expense_category_id}','{$expense_amount}','{$expense_date}','{$expense_details}')";
        $result = mysqli_query($connection, $query);
        $selectId ="SELECT id FROM expense ORDER BY id DESC limit 1";
        $idQuery = mysqli_query($connection,$selectId);
        $getId = mysqli_fetch_assoc($idQuery);
        $expId = $getId['id'];
        $insertReport = "INSERT INTO accounts (expense_id,se_date,expense_amount) VALUES ('{$expId}','{$expense_date}','{$expense_amount}')";
        $reportQuery = mysqli_query($connection,$insertReport);
        if ($result && $reportQuery) {
            $status = 13;
        }
    } else {
        $status = 14;
    }
    echo("<script>location.href = 'AddNewCosts.php?status={$status}';</script>");

}

function deleteExpense($id) {
    global $connection;
    $query = "DELETE expense,accounts FROM expense INNER JOIN accounts ON expense.id = accounts.expense_id WHERE expense.id = '{$id}'";
    $result = mysqli_query($connection,$query);
    if($result){
        $status = 15;
    }
    echo("<script>location.href = 'AddNewCosts.php?status={$status}';</script>");
}

function getAllExpense(){
    global $connection;
    $select = "SELECT expense.id,expense.expense_category_id,expense.expense_amount,expense.expense_date,expense.expense_details,expansecategory.category_name FROM expense JOIN expansecategory ON expense.expense_category_id = expansecategory.id ORDER BY expense.id DESC";
    $query = mysqli_query($connection,$select);
    return $query;
}

function updateExpenseInfo($id, $expense_category_id, $expense_amount, $expense_date,$expense_details){
    global $connection;
    $update = "UPDATE expense SET expense_category_id='{$expense_category_id}', expense_amount='{$expense_amount}', expense_date='{$expense_date}', expense_details='{$expense_details}' WHERE id = '{$id}'";
    $query = mysqli_query($connection,$update);
    if($query){
        $status = 32;
    }
    echo("<script>location.href = 'AddNewCosts.php?status={$status}';</script>");
}
function addStaff($staff_name,$staff_phone_no,$staff_address,$note,$father_name){
    global $connection;
    if ($staff_name != '' && $staff_phone_no != '' && $staff_address != '') {
        $allowType = array('image/png', 'image/jpg', 'image/jpeg');
        $checkValid = "SELECT staff_phone_no FROM staff WHERE staff_phone_no = '{$staff_phone_no}'";
        $query = mysqli_query($connection, $checkValid);
        $valid = mysqli_num_rows($query);
        if ($valid <= 0) {
            if (isset($_FILES['staff_profile_pic']) && isset($_FILES['staff_nid'])) {
                if (in_array($_FILES['staff_profile_pic']['type'], $allowType) !== false && in_array($_FILES['staff_nid']['type'], $allowType) !== false && $_FILES['staff_profile_pic']['size'] < 2 * 1024 * 1024 && $_FILES['staff_nid']['size'] < 2 * 1024 * 1024) {
                    $staff_profile_pic = $_FILES['staff_profile_pic']['name'];
                    move_uploaded_file($_FILES['staff_profile_pic']['tmp_name'], "files/" . $_FILES['staff_profile_pic']['name']);
                    $staff_nid = $_FILES['staff_nid']['name'];
                    move_uploaded_file($_FILES['staff_nid']['tmp_name'], "files/" . $_FILES['staff_nid']['name']);
                    $addEmployee = "INSERT INTO staff (staff_name,staff_phone_no,staff_address,staff_profile_pic,staff_nid,note,father_name) VALUES ('{$staff_name}','{$staff_phone_no}','{$staff_address}','{$staff_profile_pic}','{$staff_nid}','{$note}','{$father_name}')";
                    $employeeQuery = mysqli_query($connection, $addEmployee);
                } else {
                    $addEmployee = "INSERT INTO staff (staff_name,staff_phone_no,staff_address,note,father_name) VALUES ('{$staff_name}','{$staff_phone_no}','{$staff_address}','{$note}','{$father_name}')";
                    $employeeQuery = mysqli_query($connection, $addEmployee);
                    if ($employeeQuery) {
                        $status = 16;
                    }
                }
            } else {
                $status = 17;
            }

        } else {
            $status = 18;
        }
    } else {
        $status = 19;
    }
    echo("<script>location.href = 'AddNewEmployees.php?status={$status}';</script>");
}

function updateStaffInfo($staff_name,$staff_phone_no,$staff_address,$employee_id,$note,$father_name){
    global $connection;
    if($staff_name != '' && $staff_phone_no != '') {
        $allowType = array('image/png', 'image/jpg', 'image/jpeg');
        if (isset($_FILES['staff_profile_pic']) && isset($_FILES['staff_nid'])) {
            if (in_array($_FILES['staff_profile_pic']['type'], $allowType) !== false && in_array($_FILES['staff_nid']['type'], $allowType) !== false) {
                $staff_profile_pic = $_FILES['staff_profile_pic']['name'];
                $staff_nid = $_FILES['staff_nid']['name'];
                move_uploaded_file($_FILES['staff_profile_pic']['tmp_name'], "files/" . $_FILES['staff_profile_pic']['name']);
                move_uploaded_file($_FILES['staff_nid']['tmp_name'], "files/" . $_FILES['staff_nid']['name']);
                $updateEmployee = "UPDATE staff SET staff_name='{$staff_name}', staff_phone_no='{$staff_phone_no}', staff_address='{$staff_address}', staff_profile_pic='{$staff_profile_pic}', staff_nid='{$staff_nid}',note='{$note}',father_name='{$father_name}' where id = '{$employee_id}'";
                $employeeQuery = mysqli_query($connection, $updateEmployee);
                if ($employeeQuery) {
                    $status = 28;
                }
            }else {
                $updateEmployee = "UPDATE staff SET staff_name='{$staff_name}', staff_phone_no='{$staff_phone_no}', staff_address='{$staff_address}', note='{$note}',father_name='{$father_name}' where id = '{$employee_id}'";
                $employeeQuery = mysqli_query($connection, $updateEmployee);
                if ($employeeQuery) {
                    $status = 28;
                }
            }
        }
    } else {
        $status = 29;
    }
    echo("<script>location.href = 'AddNewEmployees.php?status={$status}';</script>");
}

function deleteEmployeeSalary($id){
    global $connection;
    $query = "DELETE staffsalary, expense, accounts from staffsalary LEFT join expense on expense.staff_id = staffsalary.staff_id LEFT join accounts on expense.id = accounts.expense_id WHERE staffsalary.id = '{$id}'";
    $result = mysqli_query($connection,$query);
    if($result){
        $status = 31;
    }
    echo("<script>location.href = 'AddNewEmployees.php?status={$status}';</script>");
}

function updateSalary($id, $staff_id, $salary_amount, $salary_date, $details){
    global $connection;
    $update = "UPDATE staffsalary SET staff_id='{$staff_id}', salary_amount='{$salary_amount}', salary_date='{$salary_date}', details='{$details}' WHERE id='{$id}'";
    $query = mysqli_query($connection,$update);
    if($query){
        $status = 33;
    }
    echo("<script>location.href = 'EmployeeSalary.php?status={$status}';</script>");
}

/*-------------- start new version ---------------------*/

function addNagadCategory($category_name,$category_details){
    global $connection;
    if ($category_name != '') {
        $sql = "SELECT * FROM  nagad_category WHERE categoryname = '{$category_name}'";
        $query = mysqli_query($connection,$sql);
        $row = mysqli_num_rows($query);
        if($row<=0){
            $query = "INSERT INTO  nagad_category (categoryname,description) VALUES('{$category_name}','{$category_details}')";
            $result = mysqli_query($connection, $query);
            if ($result) {
                $status = 34;
            }
        }else{
            $status = 35;
        }
    } else {
        $status = 36;
    }
    echo("<script>location.href = 'nagadCategory.php?status={$status}';</script>");
}

function getNagadCategory() {
    global $connection;
    $query = "SELECT * FROM nagad_category ORDER BY id DESC";
    $result = mysqli_query($connection, $query);
    return $result;

}


function deleteNagadCategory($id){
    global $connection;
    $query = "DELETE FROM nagad_category WHERE id = '{$id}' LIMIT 1";
    $result = mysqli_query($connection,$query);
    if($result){
        $status = 37;
    }
    echo("<script>location.href = 'nagadCategory.php?status={$status}';</script>");
}


function addNagad($nagad_category,$nagad_amount,$nagad_date,$nagad_details){
    global $connection;
    if ($nagad_amount != '' && $nagad_date != '') {

        $insertNagadAmount = "INSERT INTO nagadamount (nagad_category,nagad_amount,nagad_date,nagad_details) VALUES ('{$nagad_category}','{$nagad_amount}','{$nagad_date}','{$nagad_details}')";
        $result = mysqli_query($connection,$insertNagadAmount);
        $selectId ="SELECT id FROM nagadamount ORDER BY id DESC limit 1";
        $idQuery = mysqli_query($connection,$selectId);
        $getId = mysqli_fetch_assoc($idQuery);
        $nagadId = $getId['id'];
        $insertReport = "INSERT INTO accounts (sale_id,se_date,sale_amount) VALUES ('{$nagadId}','{$nagad_date}','{$nagad_amount}')";
        $reportQuery = mysqli_query($connection,$insertReport);
        if ($result && $reportQuery) {
            $status = 38;
        }
    } else {
        $status = 39;
    }
    echo("<script>location.href = 'addNagad.php?status={$status}';</script>");

}


function getAllNagad(){
    global $connection;
    $selectNagad = "SELECT nagadamount.id, nagadamount.nagad_category, nagadamount.nagad_amount,nagadamount.nagad_date,nagadamount.nagad_details,nagad_category.categoryname FROM nagadamount LEFT JOIN nagad_category ON nagadamount.nagad_category = nagad_category.id";
    $result = mysqli_query($connection,$selectNagad);
    return $result;
}

function updateNagadInfo($id, $nagad_category,$nagad_amount,$nagad_date,$nagad_details){
    global $connection;
    $update = "UPDATE nagadamount SET nagad_category='{$nagad_category}', nagad_amount='{$nagad_amount}', nagad_date='{$nagad_date}', nagad_details='{$nagad_details}' WHERE id = '{$id}'";
    $query = mysqli_query($connection,$update);
    if($query){
        $status = 40;
    }
    echo("<script>location.href = 'addNagad.php?status={$status}';</script>");
}

function deleteNagad($id) {
    global $connection;
    $query = "DELETE FROM nagadamount WHERE id = '{$id}'";
    $result = mysqli_query($connection,$query);
    if($result){
        $status = 41;
    }
    echo("<script>location.href = 'addNagad.php?status={$status}';</script>");
}

function bank_transaction($transaction_type,$amount,$date,$details){
    global $connection;
    if($transaction_type != '' && $amount != ''){
        $bankTransaction = "INSERT INTO bank_transaction (transaction_type, amount,date, details) VALUES ('{$transaction_type}','{$amount}','{$date}','{$details}')";
        $result = mysqli_query($connection,$bankTransaction);

        if($result){
            $status = 42;
        }
    }else{
        $status = 43;
    }
    echo("<script>location.href = 'bankTransaction.php?status={$status}';</script>");
}

function getAllBankTransaction(){
    global $connection;
    $allTransaction = "SELECT * FROM bank_transaction";
    $query = mysqli_query($connection,$allTransaction);
    return $query;
}

function updateTransaction($id, $transaction_type,$amount,$date,$details){
    global $connection;
    $update = "UPDATE bank_transaction SET transaction_type ='{$transaction_type}',amount='{$amount}',date='{$date}',details='{$details}' WHERE id = '{$id}'";
    $query = mysqli_query($connection,$update);
    if($query){
        $status = 44;
    }
    echo("<script>location.href = 'bankTransaction.php?status={$status}';</script>");
}


function deleteTransaction($id) {
    global $connection;
    $query = "DELETE FROM bank_transaction WHERE id = '{$id}'";
    $result = mysqli_query($connection,$query);
    if($result){
        $status = 45;
    }
    echo("<script>location.href = 'bankTransaction.php?status={$status}';</script>");
}

/* ------------------new version report start-----------*/

function nagadReport($categoryname,$start_date,$end_date){
    global $connection;
    $query = "SELECT nagadamount.nagad_category,nagadamount.nagad_amount,nagadamount.nagad_date,nagadamount.nagad_details,nagad_category.id,nagad_category.categoryname FROM nagadamount JOIN nagad_category ON nagadamount.nagad_category = nagad_category.id WHERE nagadamount.nagad_category = '{$categoryname}' AND nagadamount.nagad_date BETWEEN '{$start_date}' AND '{$end_date}'";
    $result = mysqli_query($connection, $query);
    return $result;
}

function bankReport($start_date,$end_date){
    global $connection;
    $query = "SELECT * FROM bank_transaction WHERE date BETWEEN '{$start_date}' AND '{$end_date}'";
    $result = mysqli_query($connection, $query);
    return $result;
}

function accountSummary($start_date,$end_date){
    global $connection;
    $report = "SELECT se_date, SUM(sale_amount) AS total_sale, SUM(expense_amount) AS total_expense FROM accounts WHERE se_date BETWEEN '{$start_date}' AND '{$end_date}' GROUP BY se_date";
    $reportQuery = mysqli_query($connection,$report);
    return $reportQuery;
}
/*-------------- end new version -----------------------*/

function getAllEmployee(){
    global $connection;
    $selectAll = "SELECT * FROM `staff` WHERE is_active = '1' ORDER BY id DESC";
    $query = mysqli_query($connection,$selectAll);
    return $query;
}

function employeeList(){
    global $connection;
    $select = "SELECT id, staff_name FROM staff";
    $query = mysqli_query($connection,$select);
    return $query;
}

function getEmployeeByEmployeeID($empoyee_id){
    global $connection;
    $selectEmployee = "SELECT * FROM `staff` WHERE staff.id='{$empoyee_id}'";
    $result = mysqli_query($connection,$selectEmployee);
    return $result;
}

function getStaffSalaryDetails($empoyee_id){
    global $connection;
    $select = "SELECT staffsalary.id,staffsalary.salary_amount,staffsalary.salary_date,staffsalary.details,staff.staff_name FROM staffsalary JOIN staff ON staffsalary.staff_id = staff.id WHERE staff.id ='{$empoyee_id}'";
    $result = mysqli_query($connection,$select);
    return $result;
}

function deleteEmployee($employee_id){
    global $connection;
    $deleteEmployee = "UPDATE staff SET is_active = 0 WHERE id = '{$employee_id}' LIMIT 1";
    $result = mysqli_query($connection,$deleteEmployee);
    if($result){
        $status = 30;
    }
    echo("<script>location.href = 'AddNewEmployees.php?status={$status}';</script>");
}

function addStaffSalary($staff_id,$salary_amount,$salary_date,$details){
    global $connection;
    if($staff_id != '' && $salary_amount != ''){
        $addSalary = "INSERT INTO staffsalary (staff_id,salary_amount,salary_date,details) VALUES ('{$staff_id}','{$salary_amount}','{$salary_date}','{$details}')";
        $query = mysqli_query($connection, $addSalary);
        if($query){
            $addExpense = "INSERT INTO expense (expense_category_id,staff_id,expense_amount,expense_date,expense_details) VALUES ('21','{$staff_id}','{$salary_amount}','{$salary_date}','{$details}')";
            $expenseQuery = mysqli_query($connection, $addExpense);
            $selectId ="SELECT id FROM expense ORDER BY id DESC limit 1";
            $idQuery = mysqli_query($connection,$selectId);
            $getId = mysqli_fetch_assoc($idQuery);
            $expId = $getId['id'];
            $insertReport = "INSERT INTO accounts (expense_id,se_date,expense_amount) VALUES ('{$expId}','{$salary_date}','{$salary_amount}')";
            $reportQuery = mysqli_query($connection,$insertReport);
            if($expenseQuery && $reportQuery) {
                $status = 20;
            }
        }
    }else{
        $status = 21;
    }
    echo("<script>location.href = 'EmployeeSalary.php?status={$status}';</script>");
}

function getAllSalary(){
    global $connection;
    $selectAll = "SELECT staffsalary.id,staffsalary.staff_id,staffsalary.salary_amount,staffsalary.salary_date,staffsalary.details,staff.staff_name FROM staffsalary JOIN staff ON staffsalary.staff_id = staff.id ORDER BY staffsalary.id DESC";
    $query = mysqli_query($connection,$selectAll);
    return $query;
}

function costReport($category_name,$start_date,$end_date){
    global $connection;
    $select = "SELECT expense.id,expense.expense_amount,expense.expense_date,expense.expense_details,expansecategory.category_name FROM expense JOIN expansecategory ON expense.expense_category_id = expansecategory.id WHERE expense.expense_category_id = '{$category_name}' AND expense.expense_date BETWEEN '{$start_date}' AND '{$end_date}'";
    $query = mysqli_query($connection,$select);
    return $query;
}

/*function saleReport($start_date,$end_date){
    global $connection;
    $query = "SELECT sale_date,sale_details, SUM(sale_amount) AS sale_amount FROM fishselling WHERE fishselling.sale_date BETWEEN '{$start_date}' AND '{$end_date}' GROUP BY sale_date";
    $result = mysqli_query($connection, $query);
    return $result;
}*/

function accountReport($start_date,$end_date){
    global $connection;
    $report = "SELECT se_date, SUM(sale_amount) AS total_sale, SUM(expense_amount) AS total_expense FROM accounts WHERE se_date BETWEEN '{$start_date}' AND '{$end_date}' GROUP BY se_date";
    $reportQuery = mysqli_query($connection,$report);
    return $reportQuery;
}

function employeeSalaryReport($start_date,$end_date) {
    global $connection;
    $select = "SELECT staffsalary.id,staffsalary.staff_id,staffsalary.salary_amount,staffsalary.salary_date,staffsalary.details,staff.staff_name,SUM(staffsalary.salary_amount) as new_salary FROM staffsalary JOIN staff ON staffsalary.staff_id = staff.id WHERE staffsalary.salary_date BETWEEN '{$start_date}' AND '{$end_date}' GROUP BY staffsalary.staff_id ORDER BY staffsalary.id DESC";
    $query = mysqli_query($connection,$select);
    return $query;
}

function employeeLastSalaryReport($employeeId) {

    global $connection;
    $select = "SELECT staffsalary.id,staffsalary.salary_amount,staffsalary.salary_date,staffsalary.details FROM staffsalary  WHERE  staffsalary.staff_id = '{$employeeId}' ORDER BY staffsalary.salary_date DESC LIMIT 1";

    $query = mysqli_query($connection,$select);
    $row = mysqli_fetch_assoc($query);
    return $row;
}

function staffSalaryReport($staff_id,$start_date, $end_date){
    global $connection;
    $select = "SELECT s1.salary_amount,s1.salary_date,s1.details, s2.staff_name,s2.staff_phone_no,s2.father_name FROM staffsalary s1 JOIN staff s2 ON s2.id = s1.staff_id WHERE s1.staff_id = '{$staff_id}' AND salary_date BETWEEN '{$start_date}' AND '{$end_date}'";
    $query = mysqli_query($connection,$select);
    return $query;
}

/*function todaySale(){
    global $connection;
    $select = "SELECT SUM(sale_amount) as sale FROM fishselling where sale_date = CURRENT_DATE()";
    $query = mysqli_query($connection,$select);
    return $query;
}*/

/*function monthlySale(){
    global $connection;
    $select = "SELECT SUM(sale_amount) as monthly_sale FROM fishselling where MONTH(sale_date) = MONTH(CURRENT_DATE())";
    $query = mysqli_query($connection,$select);
    return $query;
}*/
/*function yearlySale(){
    global $connection;
    $select = "SELECT SUM(sale_amount) as yearly_sale FROM fishselling where YEAR(sale_date) = YEAR(CURRENT_DATE())";
    $query = mysqli_query($connection,$select);
    return $query;
}*/

function todayNagad(){
    global $connection;
    $select = "SELECT SUM(nagad_amount) as amount FROM nagadamount where nagad_date = CURRENT_DATE()";
    $query = mysqli_query($connection,$select);
    return $query;
}

function monthlyNagad() {
    global $connection;
    $select = "SELECT SUM(nagad_amount) as monthly_nagad FROM nagadamount where MONTH(nagad_date) = MONTH(CURRENT_DATE())";
    $query = mysqli_query($connection, $select);
    return $query;
}

function yearlyNagad(){
    global $connection;
    $select = "SELECT SUM(nagad_amount) as yearly_nagad FROM nagadamount where YEAR(nagad_date) = YEAR(CURRENT_DATE())";
    $query = mysqli_query($connection,$select);
    return $query;
}

function todayExpense(){
    global $connection;
    $select = "SELECT SUM(expense_amount) as expense FROM expense where expense_date = CURRENT_DATE()";
    $query = mysqli_query($connection,$select);
    return $query;
}

function monthlyExpense(){
    global $connection;
    $select = "SELECT SUM(expense_amount) as monthly_expense FROM expense where MONTH(expense_date) = MONTH(CURRENT_DATE())";
    $query = mysqli_query($connection,$select);
    return $query;
}

function yearlyExpense(){
    global $connection;
    $select = "SELECT SUM(expense_amount) as yearly_expense FROM expense where YEAR(expense_date) = YEAR(CURRENT_DATE())";
    $query = mysqli_query($connection,$select);
    return $query;
}

function bankDeposited(){
    global $connection;
    $selectData = "SELECT SUM(amount) as depisoted FROM bank_transaction WHERE transaction_type = 'Deposited'";
    $query = mysqli_query($connection,$selectData);
    return $query;
}

function bankWithDraw (){
    global $connection;
    $selectData = "SELECT SUM(amount) as withdraw FROM bank_transaction WHERE transaction_type = 'Withdraw'";
    $query = mysqli_query($connection,$selectData);
    return $query;
}







