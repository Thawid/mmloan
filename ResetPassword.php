<?php
    session_start();
    require_once 'functions.php';
    Authorization();
    include_once 'config.php';
    include "header.php";
    $action = $_POST['action'] ?? '';
    $status = 0;
    if ('changePassword' == $action) {
        $current_password = filter_input(INPUT_POST, 'currentPassword', FILTER_SANITIZE_STRING);
        $password = filter_input(INPUT_POST, 'newPassword', FILTER_SANITIZE_STRING);
        changePassword($current_password, $password);
    }
    $getAdminInfo = getAdminInfo();
    $info = mysqli_fetch_assoc($getAdminInfo);
?>
<!--  BEGIN CONTENT AREA  -->
<div id="content" class="main-content">
    <div class="layout-px-spacing">
        <div class="page-header">
            <div class="page-title">
                <h3>এডমিন এর সকল তথ্য</h3>
            </div>
            <nav class="breadcrumb-one" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a></li>
                    <li class="breadcrumb-item active" aria-current="page"><span>ব্যক্তি তথ্য</span></li>
                </ol>
            </nav>
        </div>
        <div class="row layout-spacing">

            <!-- Content -->
            <div class="col-xl-4 col-lg-6 col-md-5 col-sm-12 layout-top-spacing">
                <div class="user-profile layout-spacing">
                    <div class="widget-content widget-content-area">
                        <div class="d-flex justify-content-between">
                            <h3 class="">এডমিন এর তথ্য সমূহ</h3>
                            <a href="AdminUserProfile.php" class="mt-2 edit-profile">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-edit-3">
                                    <path d="M12 20h9"></path>
                                    <path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path>
                                </svg>
                            </a>
                        </div>
                        <div class="text-center user-info">
                            <img src="files/<?php echo $info['profile_pic']; ?>" height="80" width="80" alt="avatar">
                            <p class=""><?php echo $info['user_name']; ?></p>
                        </div>
                        <div class="user-info-list">

                            <div class="">
                                <ul class="contacts-block list-unstyled">
                                    <li class="contacts-block__item">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-calendar">
                                            <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
                                            <line x1="16" y1="2" x2="16" y2="6"></line>
                                            <line x1="8" y1="2" x2="8" y2="6"></line>
                                            <line x1="3" y1="10" x2="21" y2="10"></line>
                                        </svg>
                                        <?php echo $info['birth']; ?>
                                    </li>

                                    <li class="contacts-block__item">
                                        <a href="mailto:example@mail.com">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                 stroke-linecap="round" stroke-linejoin="round"
                                                 class="feather feather-mail">
                                                <path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
                                                <polyline points="22,6 12,13 2,6"></polyline>
                                            </svg>
                                            <?php echo $info['user_email']; ?>
                                        </a>
                                    </li>
                                    <li class="contacts-block__item">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-phone">
                                            <path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>
                                        </svg>
                                        <?php echo $info['phone_no']; ?>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 col-lg-6 col-md-7 col-sm-12 layout-top-spacing">
                <div class="skills layout-spacing ">
                        <div class="widget-content widget-content-area">
                            <h3 class="">পাসওয়ার্ড পরিবর্তন করুন</h3>
                            <div class="row">
                                <div class="col-lg-12">
                                    <form name="frmChange" method="post" action="" onSubmit="return validatePassword()">
                                        <div style="width:500px;">
                                            <?php
                                            $status = $_GET['status']??0;
                                            if(23 == $status){   ?>
                                                <div class="row">
                                                    <div class="col-10 offset-sm-8">
                                                        <div class="alert alert-info mb-4" role="alert">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                                            </button>
                                                            <strong>Well Done !!</strong> <?php echo getStatusMessage($status); ?></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } elseif(24 == $status) { ?>
                                                <div class="row">
                                                    <div class="col-10 offset-sm-8">
                                                        <div class="alert alert-warning mb-4" role="alert">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                                            </button>
                                                            <strong>Warning !! </strong> <?php echo getStatusMessage($status); ?></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }  ?>
                                            <table border="0" cellpadding="10" cellspacing="0" width="800" align="center" class="tblSaveForm">
                                                <tr>
                                                    <td width="40%"><label>Current Password</label></td>
                                                    <td width="60%"><input type="password" name="currentPassword" class="form-control mb-4"><span id="currentPassword"></span></td>
                                                </tr>
                                                <tr>
                                                    <td><label>New Password</label></td>
                                                    <td><input type="password" name="newPassword" class="form-control mb-4"><span id="newPassword"></span></td>
                                                </tr>
                                                <tr>
                                                    <td><label>Confirm Password</label></td>
                                                    <td><input type="password" name="confirmPassword" class="form-control mb-4"><span id="confirmPassword"></span></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <input type="submit" name="submit" value="Submit" class="btn btn-success">
                                                        <input type="hidden" name="action" id="action" value="changePassword">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                </div>
        </div>

    </div>

</div>
<?php include "footer.php"; ?>
    <script>
        function validatePassword() {
            var currentPassword,newPassword,confirmPassword,output = true;

            currentPassword = document.frmChange.currentPassword;
            newPassword = document.frmChange.newPassword;
            confirmPassword = document.frmChange.confirmPassword;

            if(!currentPassword.value) {
                currentPassword.focus();
                document.getElementById("currentPassword").innerHTML = "Required Current Password !!";
                output = false;
            }
            else if(!newPassword.value) {
                newPassword.focus();
                document.getElementById("newPassword").innerHTML = "Required New Password !!";
                output = false;
            }
            else if(!confirmPassword.value) {
                confirmPassword.focus();
                document.getElementById("confirmPassword").innerHTML = "required";
                output = false;
            }
            if(newPassword.value != confirmPassword.value) {
                newPassword.value="";
                confirmPassword.value="";
                newPassword.focus();
                document.getElementById("confirmPassword").innerHTML = "Incorrect Confirm Password !!";
                output = false;
            }
            return output;
        }
    </script>