<?php
session_start();
require_once 'functions.php';
require_once 'loan-function.php';
Authorization();
include_once 'config.php';
include "header.php";
global $connection;
$connection = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
mysqli_set_charset($connection, "utf8");
if (!$connection) {
    echo mysqli_error($connection);
    throw new Exception("Cannot connect to database");
}
$data = array();
$select = "SELECT loan_person.person_name,loan_person.person_phone_no,loan_person.person_type,loan_bill.TotalBill,loan_credit.TotalCredit FROM loan_person INNER JOIN ( SELECT person_id,SUM(billAmount) as TotalBill FROM loan_bill where create_at BETWEEN '2020-12-01' AND '2020-12-14' GROUP BY person_id ) loan_bill ON loan_person.id = loan_bill.person_id INNER JOIN ( SELECT person_id, SUM(credit_amount) AS TotalCredit FROM loan_credit WHERE create_at BETWEEN '2020-12-01' AND '2020-12-14' GROUP BY person_id ) loan_credit ON loan_person.id = loan_credit.person_id ORDER BY loan_person.person_type DESC";
$result = mysqli_query($connection,$select);
while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $data[] = $row;
}

$rows = (int) ceil(count($data)/2);
$respose = '<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
        <div class="widget-content-area br-4">
            <div class="skills layout-spacing ">

                <div class="invoice-container">
                    <div id="printableArea">
                        <div class="invoice-inbox">
                            <div id="ct" class="">
                                <div class="invoice-00001">
                                    <div class="content-section  animated animatedFadeInUp fadeInUp">
                                        <div class="row inv--head-section">
                                            <div class="col-sm-6 col-12">
                                                <h3 class="in-heading"> ববকেয়া  রিপোর্ট </h3>
                                            </div>
                                            <div class="col-sm-6 col-12 align-self-center text-sm-right">
                                                <div class="company-info">
    
                                                    <h4 class="inv-brand-name">এফ.বি:মা মরিয়ম ১</h4>
                                                </div>
                                            </div>
    
                                        </div>
    
                                        <div class="row inv--detail-section">
    
                                            <div class="col-sm-7 align-self-center">
                                                <p class="inv-to"></p>
                                            </div>
                                            <div class="col-sm-5 align-self-center  text-sm-right order-sm-0 order-1">
                                                <p class="inv-detail-title">পরিচালক:জালাল আহম্মদ
                                                    ইমরুল কায়েস রুবেল : ৩০৫/ মা মরিয়ম স্টোর,  <br> ফিসারীঘাট,ইকবাল রোড,কতোয়ালী,চট্রগ্রাম । <br>০১৮১৪১২৫৮৮৫,০১৮৬৪৯৭৬৮৫১</p>
    
                                            </div>
    
                                            <div class="col-sm-7 align-self-center">
                                           
                                                <p><b> '."$date_one".' </b> থেকে <b> '."$date_two".' </b> পর্যন্ত বকেয়া  রিপোর্ট</p>
    
                                            </div>
                                            <div class="col-sm-5 align-self-center  text-sm-right order-2">
                                                <p class="inv-created-date"><span class="inv-title">তারিখ: </span> <span class="inv-date">'."$date".'</span></p>
    
                                            </div>
                                        </div>
    
                                        <div class="row inv--product-table-section  mt-4">
                                            <div class="col-12">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead class="">
                                                        <tr>
    
                                                            <th>sl</th>
                                                            <th>ব্যাক্তির নাম</th>
                                                           
                                                            <th> জমা</th>
                                                            <th> বকেয়া</th>
                                                             <th>sl</th>
                                                            <th>ব্যাক্তির নাম</th>
                                                           
                                                            <th> জমা</th>
                                                            <th> বকেয়া</th>
                                                            
                                                           
                                                        </tr>
                                                        </thead>
                                                        <tbody>';
                                                            for ($i=0; $i < $rows; $i++) {
                                                                $no = $i + 1;
                                                                $respose .= '<tr>';
                                                                $respose .= "<td>{$no}</td>";
                                                                $respose .= "<td>{$data[$i]['person_name']}</td>";
                                                                $respose .= "<td>{$data[$i]['TotalBill']}</td>";
                                                                $respose .= "<td>{$data[$i]['TotalCredit']}</td>";
                                                                if (array_key_exists($i + $rows, $data)) {
                                                                    $no = $i + $rows + 1;
                                                                    $respose .= "<td>{$no}</td>";
                                                                    $respose .= "<td>{$data[$i + $rows]['person_name']}</td>";
                                                                    $respose .= "<td>{$data[$i + $rows]['TotalBill']}</td>";
                                                                    $respose .= "<td>{$data[$i + $rows]['TotalCredit']}</td>";
                                                                } else {
                                                                    $respose .= "<td>&nbsp;</td>";
                                                                    $respose .= "<td>&nbsp;</td>";
                                                                    $respose .= "<td>&nbsp;</td>";
                                                                    $respose .= "<td>&nbsp;</td>";
                                                                }
                                                                echo "</tr>\n";
                                                            }

                                                            $respose .= '</tbody>';
                                                            $respose .= '
                                                            <tfoot>
                                                                    <tr>
                                                                    <th></th>
                                                                    <th></th>
                                                                    <th></th>
                                                                     </tfoot>';

$respose .= '</table>';


echo $respose;
?>
<?php include "footer.php"; ?>
<script>
    $('#zero-config').DataTable({
        "oLanguage": {
            "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Results :  _MENU_",
        },
        "stripeClasses": [],
        "lengthMenu": [7, 10, 20, 50],
        "pageLength": 7
    });
</script>
