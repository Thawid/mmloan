<?php

error_reporting(0);
date_default_timezone_set('Asia/Dhaka');
include_once "config.php";
global $connection;
$connection = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
mysqli_set_charset($connection, "utf8");
if (!$connection) {
    echo mysqli_error($connection);
    throw new Exception("Cannot connect to database");
}

function br(){
    echo "<br>";
}
function dd($n){
    print_r($n);
    die();
}
function pre(){
    echo "<pre>";
}

function getStatusMessage($statusCode = 0)
{
    $status = [
        '0' => '',
        '1' => 'Duplicate Email Address',
        '2' => 'Username or Password Empty',
        '3' => 'User created successfully',
        '4' => 'Username and password didn\'t match',
        '5' => 'Username doesn\'t exist',
        '6' => 'New person added successfully',
        '7' => 'Phone number already exits',
        '8' => 'Person name & phone number can\'t be empty',
        '9' => 'Person data update successfully',
        '10' => 'Person delete successfully',
        '11' => 'Successfully add bill',
        '12' => 'Bill deleted successfully',
        '13' => 'Bill update successfully',
        '14' => 'Description and amount can\'t be empty',
        '15' => 'Credit successfully added',
        '16' => 'Field can\'t be empty',
        '17' => 'Credit successfully updated',
        '18' => 'Some thing went wrong while updated',
        '19' => 'Credit deleted successfully',
        '20' => 'Remember successfully added',
        '21' => 'Field can\'t be empty',
        '22' => 'Remember Update Successfully',
        '23' => 'Remember Delete Successfully',

    ];

    return $status[$statusCode];
}

function addPerson($person_name, $person_phone_no,$person_type){
    global $connection;

    if ($person_name != '' && $person_phone_no != '') {
        $checkValid = "SELECT person_phone_no FROM loan_person WHERE person_phone_no = '{$person_phone_no}'";
        $query= mysqli_query($connection,$checkValid);
        $valid = mysqli_num_rows($query);

        if ($valid == 0) {
            $addPerson = "INSERT INTO loan_person (person_name,person_phone_no,person_type) VALUES ('{$person_name}','{$person_phone_no}','{$person_type}')";
            $personQuery = mysqli_query($connection,$addPerson);
            if($personQuery){
                $status = 6;
            }

        } else {
            $status = 7;
        }
    } else {
        $status = 8;
    }
    echo("<script>location.href = 'add-person.php?status={$status}';</script>");
}

function getAllPersong(){
    global $connection;
    $select = "SELECT * FROM loan_person where is_active = '1' ORDER BY id DESC";
    $query = mysqli_query($connection,$select);
    return $query;
}

function getPersonById($id){
    global $connection;
    $select = "SELECT person_name, person_phone_no FROM loan_person WHERE id='{$id}'";
    $query = mysqli_query($connection,$select);
    dd($query);
}


function updatePerson($person_name,$person_phone_no,$person_id,$person_type){
    global $connection;
    if($person_name != '' && $person_phone_no != '') {
        $updatePerson = "UPDATE loan_person SET person_name='{$person_name}', person_phone_no='{$person_phone_no}',person_type='{$person_type}' where id = '{$person_id}'";
        //dd($updatePerson);
        $updateQuery = mysqli_query($connection, $updatePerson);
        if ($updateQuery) {
            $status = 9;
        }
    } else {
        $status = 8;
    }
    echo("<script>location.href = 'add-person.php?status={$status}';</script>");
}

function deletePerson($person_id){
    global $connection;
    $deleteEmployee = "UPDATE loan_person SET is_active = 0 WHERE id = '{$person_id}' LIMIT 1";
    $result = mysqli_query($connection,$deleteEmployee);
    if($result){
        $status = 10;
    }
    echo("<script>location.href = 'add-person.php?status={$status}';</script>");
}


function addBill($person_id,$description,$bill_amount,$billDate){
    global $connection;
    $description = $_POST['billDesc'];
    $bill_amount = $_POST['billAmount'];
    //$date = $_POST['billDate'];
    if(is_array($description) && count($description) != 0 && count($bill_amount) != 0){
        $desc = count($description);
        $bill = count($bill_amount);
        if($desc == $bill){
            for($x = 0; $x < $desc; $x++){
                $str[].= "({$person_id},'{$description[$x]}','{$bill_amount[$x]}','{$billDate}')";
            }
            $s = implode(',',$str);
            $sql = "INSERT INTO loan_bill (person_id,billDesc,billAmount,billDate) VALUES  $s";
            $query = mysqli_query($connection,$sql);
            if($query){
                $status = 11;
            }
        }
    }
    echo("<script>location.href = 'add-bill.php?status={$status}';</script>");
}


function getAllBill(){
    global $connection;
    //$select = "SELECT * FROM loan_bill ORDER BY id DESC";
    $select = "SELECT loan_bill.billDesc,loan_bill.billAmount,loan_person.id,loan_person.person_name FROM loan_bill INNER JOIN loan_person WHERE loan_bill.person_id = loan_person.id";
    $query = mysqli_query($connection,$select);
    return $query;
}

function deleteBill($id){
    global $connection;
    $delete = "DELETE FROM loan_bill where id='{$id}'";
    $query = mysqli_query($connection,$delete);
    if($query){
        $status = 12;
    }
    echo("<script>location.href = 'add-bill.php?status={$status}';</script>");
}

function updateBill($id,$billDesc,$billAmount){
    global $connection;
    if($billDesc != '' && $billAmount != ''){
    $update = "UPDATE loan_bill SET billDesc='{$billDesc}', billAmount='{$billAmount}' WHERE id='{$id}' LIMIT 1";
    $query = mysqli_query($connection,$update);
        if ($query) {
            $status = 13;
        }
    }else{
        $status = 14;
    }
    echo("<script>location.href = 'add-bill.php?status={$status}';</script>");
}

function addCredit($person_id,$credit_date,$credit_amount,$reference){
    global $connection;
    if($person_id != '' && $credit_amount != '' && $credit_date != '' && $reference != ''){
        $insert = "INSERT INTO loan_credit (person_id,credit_date,credit_amount,reference) VALUES ('{$person_id}','{$credit_date}','{$credit_amount}','{$reference}')";
        $query = mysqli_query($connection,$insert);
        if($insert){
            $status = 15;
        }
    }else{
        $status = 16;
    }
    echo("<script>location.href = 'add-credit.php?status={$status}';</script>");
}

function getAllCredit(){
    global $connection;
    $select = "SELECT loan_credit.id,loan_credit.person_id,loan_credit.credit_date,loan_credit.credit_amount,loan_credit.reference,loan_person.person_name FROM loan_credit INNER JOIN loan_person ON loan_credit.person_id = loan_person.id WHERE loan_credit.credit_date < CURDATE() ORDER BY loan_credit.id DESC";
    $query = mysqli_query($connection,$select);
    return $query;
}

function updateCredit($id,$person_id,$credit_date,$credit_amount,$reference){
    global $connection;
    $update = "UPDATE loan_credit SET person_id='{$person_id}', credit_date='{$credit_date}',credit_amount='{$credit_amount}', reference='{$reference}' WHERE id ='{$id}'";
    $query = mysqli_query($connection,$update);
    if($query){
        $status = 17;
    }else{
        $status = 18;
    }
    echo("<script>location.href = 'add-credit.php?status={$status}';</script>");
}

function deleteCredit($id){
    global $connection;
    $delete = "DELETE FROM loan_credit where id='{$id}'";
    $query = mysqli_query($connection,$delete);
    if($query){
        $status = 19;
    }
    echo("<script>location.href = 'add-credit.php?status={$status}';</script>");
}

function getDueList(){
    global $connection;
    $bill = "SELECT loan_person.person_name,loan_person.person_phone_no,loan_person.person_type,loan_bill.TotalBill,loan_credit.TotalCredit FROM loan_person INNER JOIN ( SELECT person_id,SUM(billAmount) as TotalBill FROM loan_bill GROUP BY person_id ) loan_bill ON loan_person.id = loan_bill.person_id INNER JOIN( SELECT person_id, SUM(credit_amount) AS TotalCredit FROM loan_credit GROUP BY person_id )loan_credit ON loan_person.id = loan_credit.person_id ORDER BY loan_person.person_type DESC";
    $dueQuery = mysqli_query($connection,$bill);
    return $dueQuery;

}

function addRemember($person_id,$receiving_date,$deposit_date,$check_number,$amount){
    global $connection;
    if($connection != '' && $receiving_date != '' && $deposit_date != ''){
        $insert = "INSERT INTO loan_remember (person_id,receiving_date,deposit_date,check_number,amount) VALUES ('{$person_id}','{$receiving_date}','{$deposit_date}','{$check_number}','{$amount}')";
        $query = mysqli_query($connection,$insert);
        if($query){
            $status = 20;
        }
    }else{
        $status = 21;
    }

    echo("<script>location.href = 'remember-list.php?status={$status}';</script>");
}

function getRemember(){
    global $connection;
    $select = "SELECT loan_person.person_name,loan_person.person_phone_no,loan_remember.id,loan_remember.person_id,loan_remember.receiving_date,loan_remember.deposit_date,loan_remember.check_number,loan_remember.amount FROM loan_person INNER JOIN loan_remember ON loan_person.id = loan_remember.person_id";
    $query = mysqli_query($connection,$select);
    return $query;
}

function updateRemember($id,$person_id,$receiving_date,$deposit_date,$check_number,$amount){
    global $connection;
    if($connection != '' && $receiving_date != '' && $deposit_date != '') {
        $update = "UPDATE loan_remember SET person_id='{$person_id}', receiving_date='{$receiving_date}',deposit_date='{$deposit_date}',check_number='{$check_number}',amount='{$amount}' where id='{$id}'";
        $query = mysqli_query($connection, $update);
        if ($query) {
            $status = 22;
        }
    }else{
        $status = 21;
    }

    echo("<script>location.href = 'remember-list.php?status={$status}';</script>");
}

function deleteRembember($id){
    global  $connection;
    $delete = "DELETE FROM loan_remember WHERE id='{$id}' LIMIT 1";
    $query = mysqli_query($connection,$delete);
    if($query){
        $status = 23;
    }
    echo("<script>location.href = 'remember-list.php?status={$status}';</script>");
}



function personCreditReport($staff_id,$start_date, $end_date){
    global $connection;
    if($staff_id != ''){
        $select = "SELECT loan_credit.id,loan_credit.person_id,loan_credit.credit_date,loan_credit.credit_amount,loan_credit.reference,loan_person.person_name,loan_person.person_phone_no FROM loan_credit INNER JOIN loan_person ON loan_credit.person_id = loan_person.id WHERE credit_date BETWEEN '{$start_date}' AND '{$end_date}' AND loan_credit.credit_date < CURDATE() AND loan_credit.person_id = '{$staff_id}'";
    }else{
        $select = "SELECT loan_credit.id,loan_credit.person_id,loan_credit.credit_date,loan_credit.credit_amount,loan_credit.reference,loan_person.person_name,loan_person.person_phone_no FROM loan_credit INNER JOIN loan_person ON loan_credit.person_id = loan_person.id WHERE credit_date BETWEEN '{$start_date}' AND '{$end_date}' AND loan_credit.credit_date < CURDATE()";
    }
    $query = mysqli_query($connection,$select);

    return $query;
}

function getBillReport($staff_id,$start_date, $end_date){
    global $connection;
    if($staff_id != ''){
        $select = "SELECT loan_bill.billDesc,loan_bill.billAmount,loan_bill.create_at,loan_person.id,loan_person.person_name,loan_person.person_phone_no FROM loan_bill INNER JOIN loan_person ON loan_bill.person_id = loan_person.id WHERE loan_bill.person_id = '{$staff_id}' AND loan_bill.create_at BETWEEN '{$start_date}' AND '{$end_date}'";
    }else{
        $select = "SELECT loan_bill.billDesc,loan_bill.billAmount,loan_bill.create_at,loan_person.id,loan_person.person_name,loan_person.person_phone_no FROM loan_bill INNER JOIN loan_person ON loan_bill.person_id = loan_person.id WHERE loan_bill.create_at BETWEEN '{$start_date}' AND '{$end_date}'";
    }
    $query = mysqli_query($connection,$select);
    return $query;
}

function getDueReport($person_id,$start_date,$end_date){
    global $connection;
    if($person_id != ''){
    $bill = "SELECT loan_person.person_name,loan_person.person_phone_no,loan_person.person_type,loan_bill.TotalBill,loan_credit.TotalCredit FROM loan_person INNER JOIN ( SELECT person_id,SUM(billAmount) as TotalBill FROM loan_bill where person_id = '{$person_id}' AND create_at BETWEEN '{$start_date}' AND '{$end_date}' GROUP BY person_id ) loan_bill ON loan_person.id = loan_bill.person_id INNER JOIN ( SELECT person_id, SUM(credit_amount) AS TotalCredit FROM loan_credit where person_id = '{$person_id}' AND create_at BETWEEN '{$start_date}' AND '{$end_date}' GROUP BY person_id ) loan_credit ON loan_person.id = loan_credit.person_id ORDER BY loan_person.person_type DESC";
    }else{
        $bill = "SELECT loan_person.person_name,loan_person.person_phone_no,loan_person.person_type,loan_bill.TotalBill,loan_credit.TotalCredit FROM loan_person INNER JOIN ( SELECT person_id,SUM(billAmount) as TotalBill FROM loan_bill where create_at BETWEEN '{$start_date}' AND '{$end_date}' GROUP BY person_id ) loan_bill ON loan_person.id = loan_bill.person_id INNER JOIN ( SELECT person_id, SUM(credit_amount) AS TotalCredit FROM loan_credit WHERE create_at BETWEEN '{$start_date}' AND '{$end_date}' GROUP BY person_id ) loan_credit ON loan_person.id = loan_credit.person_id ORDER BY loan_person.person_type DESC";
    }
    //dd($bill);
    $quey = mysqli_query($connection,$bill);
    return $quey;
}

function todayBill(){
    global $connection;
    $select = "SELECT SUM(billAmount) as amount FROM loan_bill where billDate = CURRENT_DATE()";
    $query = mysqli_query($connection,$select);
    return $query;
}

function todayCredit(){
    global $connection;
    $select = "SELECT SUM(credit_amount) as amount FROM loan_credit where credit_date = CURRENT_DATE()";
    $query = mysqli_query($connection,$select);
    return $query;
}

function monthlyBill() {
    global $connection;
    $select = "SELECT SUM(billAmount) as amount FROM loan_bill where MONTH(billDate) = MONTH(CURRENT_DATE())";
    $query = mysqli_query($connection, $select);
    return $query;
}

function monthlyCredit() {
    global $connection;
    $select = "SELECT SUM(credit_amount) as amount FROM loan_credit where MONTH(credit_date) = MONTH(CURRENT_DATE())";
    $query = mysqli_query($connection, $select);
    return $query;
}

function todayRemember(){
    global $connection;
    $select = "SELECT loan_person.person_name,loan_person.person_phone_no,loan_remember.id,loan_remember.person_id,loan_remember.receiving_date,loan_remember.deposit_date,loan_remember.check_number,loan_remember.amount FROM loan_person INNER JOIN loan_remember ON loan_person.id = loan_remember.person_id where loan_remember.deposit_date = CURRENT_DATE()";
    $query = mysqli_query($connection,$select);
    return $query;
}





