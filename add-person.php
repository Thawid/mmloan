<?php
session_start();
require_once 'functions.php';
Authorization();
include_once 'config.php';
include_once 'loan-function.php';
include "header.php";
$action = $_POST['action'] ?? '';
$task = $_GET['task'] ?? '';
$tatus = 0;
if ('addNewPerson' == $action) {
    $person_name = filter_input(INPUT_POST, 'person_name', FILTER_SANITIZE_STRING);
    $person_phone_no = filter_input(INPUT_POST, 'person_phone_no', FILTER_SANITIZE_STRING);
    $person_type = filter_input(INPUT_POST, 'person_type', FILTER_SANITIZE_STRING);
    addPerson($person_name, $person_phone_no,$person_type);
}

if ('updatePerson' == $action) {
    $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
    $person_name = filter_input(INPUT_POST,'person_name', FILTER_SANITIZE_STRING);
    $person_phone_no = filter_input(INPUT_POST,'person_phone_no', FILTER_SANITIZE_STRING);
    $person_type = filter_input(INPUT_POST, 'person_type', FILTER_SANITIZE_STRING);
    updatePerson($person_name,$person_phone_no,$id,$person_type);

}

if ('delete' == $task) {
    $id = $_GET['id'];
    deletePerson($id);
}
?>
<!-- Start Content -->
<div class="layout-px-spacing">
    <!-- Start breadcrumb -->
    <div class="page-header">
        <div class="page-title">
            <h3>নতুন ব্যক্তি যোগ করুন</h3>
        </div>
        <nav class="breadcrumb-one" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
                <li class="breadcrumb-item active" aria-current="page"><span>ব্যক্তি</span></li>
            </ol>
        </nav>
    </div>
    <!-- End breadcrumb -->
    <!-- CONTENT AREA -->
    <?php
    $status = $_GET['status']??0;
    if(6 == $status){   ?>
        <div class="row">
            <div class="col-8 offset-sm-4">
                <div class="alert alert-info mb-4" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                    <strong>Well Done !!</strong> <?php echo getStatusMessage($status); ?></button>
                </div>
            </div>
        </div>
    <?php } elseif(7 == $status) { ?>
        <div class="row">
            <div class="col-8 offset-sm-4">
                <div class="alert alert-warning mb-4" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                    <strong>Warning !! </strong> <?php echo getStatusMessage($status); ?></button>
                </div>
            </div>
        </div>
    <?php } elseif(8 == $status) { ?>
        <div class="row">
            <div class="col-8 offset-sm-4">
                <div class="alert alert-warning mb-4" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                    <strong>Warning !!  </strong> <?php echo getStatusMessage($status); ?></button>
                </div>
            </div>
        </div>
    <?php } elseif(9 == $status) { ?>
        <div class="row">
            <div class="col-8 offset-sm-4">
                <div class="alert alert-success mb-4" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                    <strong>Success !!  </strong> <?php echo getStatusMessage($status); ?></button>
                </div>
            </div>
        </div>
    <?php } elseif(10 == $status) { ?>
        <div class="row">
            <div class="col-8 offset-sm-4">
                <div class="alert alert-success mb-4" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                    <strong>Well Done !!  </strong> <?php echo getStatusMessage($status); ?></button>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="row layout-top-spacing">
        <div class="col-4 layout-spacing">
            <div class="widget-content-area br-4">
                <div class="widget-one">
                    <h5 class="text-center">নতুন ব্যক্তি তথ্য</h5>
                    <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" enctype="multipart/form-data">
                        <div class="form-group mb-4">
                            <label for="person_name">ব্যক্তি নাম</label>
                            <input type="text" class="form-control" id="person_name" name="person_name" placeholder="সম্পূর্ণ নামটি লিখুন">
                        </div>

                        <div class="form-group mb-4">
                            <label for="person_phone_no">মোবাইল নাম্বার </label>
                            <input type="number" class="form-control" id="person_phone_no" name="person_phone_no" placeholder="+৮৮০   x x x x x x">
                        </div>
                        <div class="form-group mb-4">
                            <label for="person_type">ব্যক্তির ধরন </label>
                            <select name="person_type" id="" class="form-control">
                                <option value="">Select Type</option>
                                <option value="*">Star</option>
                                <option value="#">Hash</option>
                            </select>
                        </div>
                        <input type="submit" name="submit" value="সাবমিট" class="btn btn-primary btn-block mb-4 mr-2">
                        <input type="hidden" name="action" id="action" value="addNewPerson">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-8 layout-spacing">
            <div class="widget-content-area br-4">
                <div class="widget-one">
                    <h5 class="text-center">ব্যক্তি তথ্য  তালিকা</h5>
                    <div class="table-responsive mb-4">
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th>সিরিয়াল</th>
                                <th>ব্যক্তি নাম</th>
                                <th>মোবাইল নাম্বার </th>
                                <th>বিস্তারিত</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $result = getAllPersong();
                            $count = 1;
                            while ($rows = mysqli_fetch_assoc($result)) {  ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><?php echo $rows['person_name']; ?></td>
                                    <td><?php echo $rows['person_phone_no']; ?></td>

                                    <td>
                                        <div class="btn-group">
                                            <a href="javascript:void(0);" type="button" class="btn btn-dark btn-sm">বিস্তারিত তথ্য</a>
                                            <button type="button" class="btn btn-dark btn-sm dropdown-toggle dropdown-toggle-split" id="dropdownMenuReference1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuReference1">

                                                <a type="button" class="dropdown-item" data-toggle="modal" data-target="#peopleUpdate<?php echo $rows['id'];?>">আপডেট </a>
                                                <?php printf("<a class='dropdown-item delete' href='add-person.php?task=delete&id=%s' onclick='return confirmDelete()'>ডিলিট</a>", $rows['id']) ?>

                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <div class="modal fade" id="peopleUpdate<?php echo $rows['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalCenterTitle">ব্যক্তি তথ্য</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="widget-one">
                                                    <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">

                                                        <div class="form-group mb-4">
                                                            <label for="people_name"> Name </label>
                                                            <input id="person_name" type="text" name="person_name" value="<?php echo $rows['person_name']; ?>"
                                                                   class="form-control" required="">
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <label for="person_phone_no">Phone </label>
                                                            <input type="text" id="person_phone_no" name="person_phone_no"
                                                                   class="form-control"  value="<?php echo $rows['person_phone_no']; ?>">
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <label for="person_type">ব্যক্তির ধরন </label>
                                                            <select name="person_type" id="" class="form-control">
                                                                <option value="*" <?php if($rows['person_type'] == ''){ echo 'selected';}?> >Select Type</option>
                                                                <option value="*" <?php if($rows['person_type'] == '*'){ echo 'selected';}?> >Star</option>
                                                                <option value="#" <?php if($rows['person_type'] == '#'){echo 'selected';}?>>Hash</option>
                                                            </select>
                                                        </div>
                                                        <input type="submit" name="submit" value="সাবমিট" class="btn btn-primary btn-block mb-4 mr-2">
                                                        <input type="hidden" name="id"  value="<?php echo $rows['id']; ?>">
                                                        <input type="hidden" name="action" id="action" value="updatePerson">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                $count++;
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- End Content -->
<?php include "footer.php"; ?>

<script>
    function confirmDelete() {
        if (confirm("Are you sure want to delete?")) {
            return true;
        }
        return false;
    }
</script>