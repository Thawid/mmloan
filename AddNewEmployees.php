<?php
    session_start();
    require_once 'functions.php';
    Authorization();
    include_once 'config.php';
    include "header.php";
    $action = $_POST['action'] ?? '';
    $task = $_GET['task'] ?? '';
    $tatus = 0;
    if ('addNewEmployee' == $action) {
        $staff_name = filter_input(INPUT_POST, 'staff_name', FILTER_SANITIZE_STRING);
        $staff_phone_no = filter_input(INPUT_POST, 'staff_phone_no', FILTER_SANITIZE_STRING);
        $staff_address = filter_input(INPUT_POST, 'staff_address', FILTER_SANITIZE_STRING);
        $note = filter_input(INPUT_POST, 'note', FILTER_SANITIZE_STRING);
        $father_name = filter_input(INPUT_POST, 'father_name', FILTER_SANITIZE_STRING);
        addStaff($staff_name, $staff_phone_no, $staff_address,$note,$father_name);
    }

    if ('delete' == $task) {
        $id = $_GET['id'];
        deleteEmployee($id);
    }
?>
			<!-- Start Content -->
			<div class="layout-px-spacing">
				<!-- Start breadcrumb -->
				<div class="page-header">
					<div class="page-title">
						<h3>নতুন কর্মচারী যোগ করুন</h3>
					</div>
					<nav class="breadcrumb-one" aria-label="breadcrumb">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="index.php"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
							<li class="breadcrumb-item active" aria-current="page"><span>কর্মচারী</span></li>
						</ol>
					</nav>
				</div>
				<!-- End breadcrumb -->
                <!-- CONTENT AREA -->
                <?php
                $status = $_GET['status']??0;
                if(16 == $status){   ?>
                    <div class="row">
                        <div class="col-8 offset-sm-4">
                            <div class="alert alert-info mb-4" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                </button>
                                <strong>Well Done !!</strong> <?php echo getStatusMessage($status); ?></button>
                            </div>
                        </div>
                    </div>
                <?php } elseif(17 == $status) { ?>
                    <div class="row">
                        <div class="col-8 offset-sm-4">
                            <div class="alert alert-warning mb-4" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                </button>
                                <strong>Warning !! </strong> <?php echo getStatusMessage($status); ?></button>
                            </div>
                        </div>
                    </div>
                <?php } elseif(18 == $status) { ?>
                    <div class="row">
                        <div class="col-8 offset-sm-4">
                            <div class="alert alert-warning mb-4" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                </button>
                                <strong>Success !!  </strong> <?php echo getStatusMessage($status); ?></button>
                            </div>
                        </div>
                    </div>
                <?php } elseif(19 == $status) { ?>
                    <div class="row">
                        <div class="col-8 offset-sm-4">
                            <div class="alert alert-warning mb-4" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                </button>
                                <strong>Warning !!  </strong> <?php echo getStatusMessage($status); ?></button>
                            </div>
                        </div>
                    </div>
                <?php } elseif(28 == $status) { ?>
                <div class="row">
                    <div class="col-8 offset-sm-4">
                        <div class="alert alert-success mb-4" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                            </button>
                            <strong>Well Done !!  </strong> <?php echo getStatusMessage($status); ?></button>
                        </div>
                    </div>
                </div>
                <?php } elseif(29 == $status) { ?>
                <div class="row">
                    <div class="col-8 offset-sm-4">
                        <div class="alert alert-warning mb-4" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                            </button>
                            <strong>Warning !!  </strong> <?php echo getStatusMessage($status); ?></button>
                        </div>
                    </div>
                </div>
               <?php } elseif(30 == $status) { ?>
                <div class="row">
                    <div class="col-8 offset-sm-4">
                        <div class="alert alert-success mb-4" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                            </button>
                            <strong>Well Done !!  </strong> <?php echo getStatusMessage($status); ?></button>
                        </div>
                    </div>
                </div>
                <?php } elseif(31 == $status) { ?>
                <div class="row">
                    <div class="col-8 offset-sm-4">
                        <div class="alert alert-success mb-4" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                            </button>
                            <strong>Well Done !!  </strong> <?php echo getStatusMessage($status); ?></button>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <div class="row layout-top-spacing">
                    <div class="col-4 layout-spacing">
                        <div class="widget-content-area br-4">
                            <div class="widget-one">
								<h5 class="text-center">নতুন কর্মচারী তথ্য</h5>
                                <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" enctype="multipart/form-data">
									<div class="form-group mb-4">
										<label for="username">কর্মচারী নাম</label>
										<input type="text" class="form-control" id="staff_name" name="staff_name" placeholder="সম্পূর্ণ নামটি লিখুন">
									</div>
                                    <div class="form-group mb-4">
                                        <label for="username">পিতার নাম</label>
                                        <input type="text" class="form-control" id="father_name" name="father_name" placeholder="সম্পূর্ণ নামটি লিখুন">
                                    </div>
									<div class="form-group mb-4">
										<label for="mobileNumber">মোবাইল নাম্বার </label>
										<input type="number" class="form-control" id="staff_phone_no" name="staff_phone_no" placeholder="+৮৮০   x x x x x x">
									</div>
									<div class="form-group mb-4">
										<label for="userAddress">ঠিকানা </label>
										<textarea class="form-control" id="staff_address" name="staff_address" rows="1"></textarea>
									</div>
                                    <div class="form-group mb-4">
                                        <label for="userAddress"> নোট </label>
                                        <textarea class="form-control" id="note" name="note" rows="1"></textarea>
                                    </div>
									<div class="form-group mb-4">
										<div class="upload mt-4 pr-md-4">
                                            <input type="file" id="input-file-max-fs" name="staff_profile_pic" class="dropify" data-default-file="assets/img/200x200.jpg" data-max-file-size="2M" />
                                            <p class="mt-2"><i class="flaticon-cloud-upload mr-1"></i> আপলোড ছবি</p>
                                        </div>
									</div>
                                    <div class="form-group mb-4">
										<div class="upload mt-4 pr-md-4">
                                            <input type="file" id="input-file-max-fs" name="staff_nid" class="dropify" data-default-file="assets/img/200x200.jpg" data-max-file-size="2M" />
                                            <p class="mt-2"><i class="flaticon-cloud-upload mr-1"></i> আপলোড NID </p>
                                        </div>
									</div>
                                    <input type="submit" name="submit" value="সাবমিট" class="btn btn-primary btn-block mb-4 mr-2">
                                    <input type="hidden" name="action" id="action" value="addNewEmployee">
								</form>
                            </div>
                        </div>
                    </div>
					<div class="col-8 layout-spacing">
                        <div class="widget-content-area br-4">
                            <div class="widget-one">
							<h5 class="text-center">কর্মচারী তথ্য  তালিকা</h5>
                            <div class="table-responsive mb-4">
                                <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                                    <thead>
                                        <tr>
											<th>সিরিয়াল</th>
											<th>কর্মচারী নাম</th>
											<th>পিতার নাম</th>
                                            <th>মোবাইল নাম্বার </th>
                                            <th>বিস্তারিত</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $result = getAllEmployee();
                                    $count = 1;
                                    while ($rows = mysqli_fetch_assoc($result)) {  ?>
                                        <tr>
											<td><?php echo $count; ?></td>
                                            <td><?php echo $rows['staff_name']; ?></td>
                                            <td><?php echo $rows['father_name']; ?></td>
                                            <td><?php echo $rows['staff_phone_no']; ?></td>
                                            <td>
                                               <div class="btn-group">
                                                    <a href="<?php printf("UserProfile.php?employee_id=%s",$rows['id']);?>" type="button" class="btn btn-dark btn-sm">বিস্তারিত তথ্য</a>
                                                    <button type="button" class="btn btn-dark btn-sm dropdown-toggle dropdown-toggle-split" id="dropdownMenuReference1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                                                    </button>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuReference1">
                                                        <?php printf("<a class='dropdown-item delete' href='UserProfile.php?task=delete&id=%s' onclick='return confirmDelete()'>ডিলিট</a>", $rows['id']) ?>

                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                        $count++;
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<!-- End Content -->
<?php include "footer.php"; ?>
<script>
    function confirmDelete() {
        if (confirm("Are you sure want to delete?")) {
            return true;
        }
        return false;
    }
</script>