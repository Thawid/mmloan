<?php
require_once '../config.php';
require_once '../functions.php';
?>
<style>
    .inv--thankYou {
        margin-top: 100px;
    }

    .table > tbody > tr > td {
        border: none;
        color: #000000;
        font-size: 16px;
        letter-spacing: 1px;
        font-weight: 700;
    }

    h2, h3, h4, h5, h6, p, a, tr, th, td {
        color: #000000;
    }
</style>
<?php
$start_date = isset($_POST['start_date']) ? $_POST['start_date'] : null;
$end_date = isset($_POST['end_date']) ? $_POST['end_date'] : null;
$dateOne = date_create("$start_date");
$date_one =  date_format($dateOne,"jS F Y");

$dateTwo = date_create("$end_date");
$date_two = date_format($dateTwo,"jS F Y");
$date = date('m-d-Y');
$respose = '<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
        <div class="widget-content-area br-4">
            <div class="skills layout-spacing ">

                <div class="invoice-container">
                    <div id="printableArea">
                        <div class="invoice-inbox">
                            <div id="ct" class="">
                                <div class="invoice-00001">
                                    <div class="content-section  animated animatedFadeInUp fadeInUp">
                                        <div class="row inv--head-section">
                                            <div class="col-sm-6 col-12">
                                                <h3 class="in-heading">একাউন্ট রিপোর্ট </h3>
                                            </div>
                                            <div class="col-sm-6 col-12 align-self-center text-sm-right">
                                                <div class="company-info">
    
                                                    <h4 class="inv-brand-name">এফ.বি:মা মরিয়ম ১</h4>
                                                </div>
                                            </div>
    
                                        </div>
    
                                        <div class="row inv--detail-section">
    
                                            <div class="col-sm-7 align-self-center">
                                                <p class="inv-to"></p>
                                            </div>
                                            <div class="col-sm-5 align-self-center  text-sm-right order-sm-0 order-1">
                                                <p class="inv-detail-title">পরিচালক:জালাল আহম্মদ
                                                    ইমরুল কায়েস রুবেল : ৩০৫/ মা মরিয়ম স্টোর,  <br> ফিসারীঘাট,ইকবাল রোড,কতোয়ালী,চট্রগ্রাম । <br>০১৮১৪১২৫৮৮৫,০১৮৬৪৯৭৬৮৫১</p>
    
                                            </div>
    
                                            <div class="col-sm-7 align-self-center">
                                                <p><b> '."$date_one".' </b> থেকে <b> '."$date_two".' </b> পর্যন্ত  একাউন্ট রিপোর্ট </p>
    
                                            </div>
                                            <div class="col-sm-5 align-self-center  text-sm-right order-2">
                                                <p class="inv-created-date"><span class="inv-title">তারিখ: </span> <span class="inv-date">'."$date".'</span></p>
    
                                            </div>
                                        </div>
    
                                        <div class="row inv--product-table-section  mt-4">
                                            <div class="col-12">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead class="">
                                                        <tr>
    
                                                            <th scope="col">ক্রঃ নং</th>
                                                            <th class="text-center" scope="col">তারিখ</th>
                                                            <th class="text-center" scope="col">নগদ  টাকার পরিমান</th>
                                                            <th class="text-center" scope="col">খরচ পরিমান</th>
                                                            <th class="text-center" scope="col">একাউন্টে জমা</th>
    
                                                        </tr>
                                                        </thead>
                                                        <tbody>';
                                                        $count = 1;
                                                        $result = accountReport($start_date, $end_date);
                                                        while ($rows = mysqli_fetch_assoc($result)) {
                                                            $sl = bn($count);
                                                            $org_date = $rows['se_date'];
                                                            $timestamp = strtotime($org_date);
                                                            $date =  date('d/m/Y', $timestamp);
                                                            $sale_amount = bn(number_format($rows['total_sale']));
                                                            $expense_amount = bn(number_format($rows['total_expense']));
                                                            $cash_InHand = bn(number_format($rows['total_sale'] - $rows['total_expense']));
                                                            $saveInHand = $rows['total_sale'] - $rows['total_expense'];

                                                            @$total_sale += $rows['total_sale'];
                                                            $totalSale = bn(number_format((float)$total_sale));
                                                            @$total_expense += $rows['total_expense'];
                                                            $totalExpense = bn(number_format((float)$total_expense));
                                                            @$totalSaveInHand += $saveInHand;
                                                            $total_cash_in_hand = bn(number_format((float)$totalSaveInHand));
                                                            $respose .= '<tr>';
                                                            $respose .= '<td class="text-left">'."$sl".'</td>';
                                                            $respose .= '<td class="text-center">' . "$date" . ' </td>';
                                                            $respose .= '<td class="text-center">' . "$sale_amount" . ' </td>';
                                                            $respose .= '<td class="text-center">' . "$expense_amount" . ' </td>';
                                                            $respose .= '<td class="text-center">' . "$cash_InHand" . ' </td>';
                                                            $respose .= '</tr>';
                                                            $count++;
                                                        }
                                                        $respose .= '</tbody>
                                                            <tfoot>
                                                                <tr>
                                                                    <th></th>
                                                                    <th></th>
                                                                    <th class="text-center">মোট : ' . "$totalSale" . ' টাকা</th>
                                                                    <th class="text-center">মোট : ' . "$totalExpense" . ' টাকা</th>
                                                                    <th class="text-center">মোট : ' . "$total_cash_in_hand" . ' টাকা</th>
                                                                    
                                                                </tr>
                                                                <tr><td colspan="5"></td></tr>
                                                                <tr><th colspan="5"></th></tr>
                                                                <tr><th colspan="5"></th></tr>
                                                                <tr>
                                                                    
                                                                    <th class="text-center"><h5> হিসাবরক্ষক </h5></th>
                                                                    <th></th>
                                                                    <th class="text-center">   <h5> ক্যাশিয়ার</h5></th>
                                                                    <th></th>
                                                                    <th class="text-center"> <h5> পরিচালক </h5></th>
                                                                </tr>
                                                            </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                  
                </div>
            </div>
        </div>
    </div>
</div>';

echo $respose;

?>
<div class="row">
    <div class="col-12">
        <button class="btn btn-primary mb-2" onclick="printDiv('printableArea')">তথ্যগুলো
            প্রিন্ট করুন
        </button>
    </div>
</div>
<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>
