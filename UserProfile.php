<?php
    session_start();
    include_once 'config.php';
    require_once 'functions.php';
    Authorization();
    $action = $_POST['action'] ?? '';
    $task = $_GET['task'] ?? '';
    if ('updateStaffInfo' == $action) {
        $employee_id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $staff_name = filter_input(INPUT_POST, 'staff_name', FILTER_SANITIZE_STRING);
        $staff_phone_no = filter_input(INPUT_POST, 'staff_phone_no', FILTER_SANITIZE_STRING);
        $staff_address = filter_input(INPUT_POST, 'staff_address', FILTER_SANITIZE_STRING);
        $note = filter_input(INPUT_POST, 'note', FILTER_SANITIZE_STRING);
        $father_name = filter_input(INPUT_POST, 'father_name', FILTER_SANITIZE_STRING);
        updateStaffInfo($staff_name, $staff_phone_no, $staff_address, $employee_id,$note,$father_name);
    }
    if ('delete' == $task) {
        $id = $_GET['id'];
        deleteEmployeeSalary($id);
    }
?>
<?php  include "header.php"; ?>
        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="page-header">
					<div class="page-title">
						<h3>একজন গ্রাহকের সম্পূর্ণ তথ্য সমূহ</h3>
					</div>
					<nav class="breadcrumb-one" aria-label="breadcrumb">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="index.php"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
							<li class="breadcrumb-item"><a href="javascript:void(0);">ব্যক্তি</a></li>
							<li class="breadcrumb-item active" aria-current="page"><span>ব্যক্তি তথ্য</span></li>
						</ol>
					</nav>
				</div>
                <div class="row layout-spacing">
                    <!-- Content -->
                    <?php
                        $empoyee_id = filter_input(INPUT_GET,'employee_id',FILTER_SANITIZE_STRING);
                        $result = getEmployeeByEmployeeID($empoyee_id);
                        $row = mysqli_fetch_assoc($result);
                    ?>
                    <div class="col-xl-4 col-lg-6 col-md-5 col-sm-12 layout-top-spacing">
                        <div class="user-profile layout-spacing">
                            <div class="widget-content widget-content-area">
                                <div class="d-flex justify-content-between">
                                    <h3 class="">ব্যক্তিগত তথ্য সমূহ</h3>
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModalCenter<?php echo $row['id']; ?>" class="mt-2 edit-profile"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-3"><path d="M12 20h9"></path><path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path></svg></a>
                                </div>
                                <div class="text-center user-info">
                                    <img src="files/<?php echo $row['staff_profile_pic']; ?>" height="80" width="80" alt="avatar">
                                    <p class=""><?php echo $row['staff_name']; ?></p>
                                </div>
                                <div class="user-info-list">
                                    <div class="">
                                        <ul class="contacts-block list-unstyled">
                                            <li class="contacts-block__item">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone"><path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg>
                                                <?php echo $row['staff_phone_no']; ?>
                                            </li>
                                        </ul>
                                    </div>                                    
                                </div>
                            </div>
                        </div>

                        <div class="education layout-spacing ">
                            <div class="widget-content widget-content-area">
                                <h3 class=""> পিতার নাম</h3>
                                <p><?php echo $row['father_name']; ?></p><br>
                                <h3 class=""> বর্তমান ঠিকানা</h3>
                                <p><?php echo $row['staff_address']; ?></p><br>
                                <h3 class=""> নোট </h3>
                                <p><?php echo $row['note']; ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8 col-lg-6 col-md-7 col-sm-12 layout-top-spacing">
                        <div class="bio layout-spacing ">
                            <div class="widget-content widget-content-area pb-4">
                                <h3 class="">লেনদেন  হিসাব</h3>
                                <div class="table-responsive mb-4 mt-4">
                                    <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>তারিখ</th>
                                                <th>বেতন টাকার পরিমান</th>
                                                <th> মোট বেতনর পরিমান  </th>
                                                <th> অ্যাকশান  </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                         $salaryDetails = getStaffSalaryDetails($empoyee_id);
                                         while ($salary = mysqli_fetch_assoc($salaryDetails)){
                                            @$totalSalary += $salary['salary_amount'];
                                        ?>
                                            <tr>
                                                <td><?php echo $salary['salary_date']; ?></td>
                                                <td><?php echo bn(number_format($salary['salary_amount'])); ?></td>
                                                <td><?php echo bn(number_format((float)$totalSalary)); ?></td>
                                                <td><?php printf("<a class='delete' href='UserProfile.php?task=delete&id=%s' onclick='return confirmDelete()'><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-x-circle table-cancel\"><circle cx=\"12\" cy=\"12\" r=\"10\"></circle><line x1=\"15\" y1=\"9\" x2=\"9\" y2=\"15\"></line><line x1=\"9\" y1=\"9\" x2=\"15\" y2=\"15\"></line></svg></a>", $salary['id']) ?></td>
                                            </tr>
                                           <?php } ?>
                                        </tbody>
                                        
                                    </table>
                                </div>
                            </div>     
                        </div> 
                         <!-- Start Infomation -->
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="education layout-spacing ">
                                    <div class="widget-content widget-content-area">
                                        <h3 class="">এন আই ডি কার্ড </h3>
                                        <p><img  class="img-fluid" src="files/<?php echo $row['staff_nid']; ?>" width="600" height="300" alt="avatar"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Infomation -->     
                    </div>
                </div>
                </div>
        </div>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">কর্মচারী তথ্য</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                </button>
            </div>
            <div class="modal-body">
                <div class="widget-one">
                    <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" enctype="multipart/form-data">
                        <div class="form-group mb-4">
                            <label for="username">কর্মচারী নাম</label>
                            <input type="text" class="form-control" id="staff_name" name="staff_name" value="<?php echo $row['staff_name']; ?>">
                        </div>
                        <div class="form-group mb-4">
                            <label for="username">পিতার নাম</label>
                            <input type="text" class="form-control" id="father_name" name="father_name" value="<?php echo $row['father_name']; ?>">
                        </div>
                        <div class="form-group mb-4">
                            <label for="mobileNumber">মোবাইল নাম্বার </label>
                            <input type="number" class="form-control" id="staff_phone_no" name="staff_phone_no" value="<?php echo $row['staff_phone_no']; ?>">
                        </div>
                        <div class="form-group mb-4">
                            <label for="userAddress">ঠিকানা </label>
                            <textarea class="form-control" id="staff_address" name="staff_address" rows="1"><?php echo $row['staff_address']; ?></textarea>
                        </div>
                        <div class="form-group mb-4">
                            <label for="userAddress"> নোট </label>
                            <textarea class="form-control" id="note" name="note" rows="1"><?php echo $row['note']; ?></textarea>
                        </div>
                        <div class="form-group mb-4">
                            <div class="upload mt-4 pr-md-4">
                                <input type="file" id="input-file-max-fs" name="staff_profile_pic" class="dropify" data-default-file="assets/img/200x200.jpg" data-max-file-size="2M" />
                                <p class="mt-2"><i class="flaticon-cloud-upload mr-1"></i> আপলোড ছবি</p>
                            </div>
                        </div>
                        <div class="form-group mb-4">
                            <div class="upload mt-4 pr-md-4">
                                <input type="file" id="input-file-max-fs" name="staff_nid" class="dropify" data-default-file="assets/img/200x200.jpg" data-max-file-size="2M" />
                                <p class="mt-2"><i class="flaticon-cloud-upload mr-1"></i> আপলোড NID </p>
                            </div>
                        </div>
                        <input type="submit" name="submit" value="সাবমিট" class="btn btn-primary btn-block mb-4 mr-2">
                        <input type="hidden" name="id" name="id" value="<?php echo $row['id']; ?>">
                        <input type="hidden" name="action" id="action" value="updateStaffInfo">

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include "footer.php"; ?>
<script>
    function confirmDelete() {
        if (confirm("Are you sure want to delete?")) {
            return true;
        }
        return false;
    }
</script>