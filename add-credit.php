<?php
session_start();
require_once 'functions.php';
Authorization();
include_once 'config.php';
include_once 'loan-function.php';
include "header.php";
$action = $_POST['action'] ?? '';
$task = $_GET['task'] ?? '';
$tatus = 0;

if ('addCredit' == $action) {
    $person_id = filter_input(INPUT_POST, 'person_id', FILTER_SANITIZE_STRING);
    $credit_date = filter_input(INPUT_POST, 'credit_date', FILTER_SANITIZE_STRING);
    $credit_amount = filter_input(INPUT_POST, 'credit_amount', FILTER_SANITIZE_STRING);
    $reference = filter_input(INPUT_POST, 'reference', FILTER_SANITIZE_STRING);
    addCredit($person_id,$credit_date,$credit_amount,$reference);

}
if ('updateBill' == $action) {
    $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
    $person_id = filter_input(INPUT_POST, 'person_id', FILTER_SANITIZE_STRING);
    $credit_date = filter_input(INPUT_POST, 'credit_date', FILTER_SANITIZE_STRING);
    $credit_amount = filter_input(INPUT_POST, 'credit_amount', FILTER_SANITIZE_STRING);
    $reference = filter_input(INPUT_POST, 'reference', FILTER_SANITIZE_STRING);
    updateCredit($id,$person_id,$credit_date,$credit_amount,$reference);

}

if ('delete' == $task) {
    $id = $_GET['id'];
    deleteCredit($id);
}
?>

<!-- Start Content -->
<div class="layout-px-spacing">
    <!-- Start breadcrumb -->
    <div class="page-header">
        <div class="page-title">
            <h3>নতুন জমা  যোগ করুন</h3>
        </div>
        <nav class="breadcrumb-one" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-home">
                            <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                            <polyline points="9 22 9 12 15 12 15 22"></polyline>
                        </svg>
                    </a></li>
                <li class="breadcrumb-item active" aria-current="page"><span> জমা </span></li>
            </ol>
        </nav>
    </div>
    <!-- End breadcrumb -->
    <!-- CONTENT AREA -->
    <?php
    $status = $_GET['status'] ?? 0;
    if (15 == $status) { ?>
        <div class="row">
            <div class="col-8 offset-sm-4">
                <div class="alert alert-info mb-4" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-x close" data-dismiss="alert">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                    <strong>Well Done !!</strong> <?php echo getStatusMessage($status); ?></button>
                </div>
            </div>
        </div>
    <?php } elseif (16 == $status) { ?>
    <div class="row">
        <div class="col-8 offset-sm-4">
            <div class="alert alert-warning mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-x close" data-dismiss="alert">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                <strong>Warning !!</strong> <?php echo getStatusMessage($status); ?></button>
            </div>
        </div>
    </div>
    <?php } elseif (17 == $status) { ?>
        <div class="row">
            <div class="col-8 offset-sm-4">
                <div class="alert alert-info mb-4" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-x close" data-dismiss="alert">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                    <strong>Well Done !!</strong> <?php echo getStatusMessage($status); ?></button>
                </div>
            </div>
        </div>
    <?php } elseif (18 == $status) { ?>
        <div class="row">
            <div class="col-8 offset-sm-4">
                <div class="alert alert-warning mb-4" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-x close" data-dismiss="alert">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                    <strong>Warning!!</strong> <?php echo getStatusMessage($status); ?></button>
                </div>
            </div>
        </div>
    <?php } elseif (19 == $status) { ?>
        <div class="row">
            <div class="col-8 offset-sm-4">
                <div class="alert alert-info mb-4" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-x close" data-dismiss="alert">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                    <strong>Well Done!!</strong> <?php echo getStatusMessage($status); ?></button>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="row layout-top-spacing">
        <div class="col-4 layout-spacing">
            <div class="widget-content-area br-4">
                <div class="widget-one">
                    <h5 class="text-center">নতুন জমা  </h5>
                    <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>"
                          enctype="multipart/form-data">
                        <div class="form-group mb-2">
                            <label for="person_name">ব্যক্তি নাম</label>
                            <select name="person_id" id="person_id" class="form-control">
                                <option value="" selected>Select Person</option>
                                <?php
                                $person = getAllPersong();
                                while ($data = mysqli_fetch_assoc($person)) {
                                    ?>
                                    <option
                                        value="<?php echo $data['id']; ?>"><?php echo $data['person_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group mb-2">
                            <label for="date">তারিখ  </label>
                            <input type="date" class="form-control" name="credit_date">
                        </div>
                        <div class="form-group mb-2">
                            <label for="amount">টাকার পরিমান </label>
                            <input type="text" class="form-control" name="credit_amount">
                        </div>
                        <div class="form-group mb-2">
                            <label for="reference">রেফারেন্সেস</label>
                            <input type="text" class="form-control" name="reference">
                        </div>
                        <input type="submit" name="submit" value="সাবমিট" class="btn btn-primary btn-block mb-4 mr-2">
                        <input type="hidden" name="action" id="action" value="addCredit">
                    </form>

                </div>
            </div>
        </div>
        <div class="col-8 layout-spacing">
            <div class="widget-content-area br-4">
                <div class="widget-one">
                    <h5 class="text-center">জমা তালিকা</h5>
                    <div class="table-responsive mb-4">
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                            <tr>
                                <td>নং</td>
                                <td>ব্যক্তি নাম</td>
                                <td>তারিখ </td>
                                <td>টাকার পরিমান </td>
                                <td>রেফারেন্স</td>
                                <td>Action</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $data = getAllCredit();
                            $count = 1;
                            while ($creditInfo = mysqli_fetch_assoc($data)) {

                            ?>
                            <tr>
                                <td><?php echo bn($count); ?></td>
                                <td><?php echo $creditInfo['person_name']; ?></td>
                                <td><?php echo $creditInfo['credit_date']; ?></td>
                                <td><?php echo bn(number_format($creditInfo['credit_amount'])); ?></td>
                                <td><?php echo $creditInfo['reference']; ?></td>
                                <td>
                                    <?php printf("<a class='delete' href='add-credit.php?task=delete&id=%s' onclick='return confirmDelete()'><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-x-circle table-cancel\"><circle cx=\"12\" cy=\"12\" r=\"10\"></circle><line x1=\"15\" y1=\"9\" x2=\"9\" y2=\"15\"></line><line x1=\"9\" y1=\"9\" x2=\"15\" y2=\"15\"></line></svg></a>", $creditInfo['id']) ?>
                                    <a type="button" class="" data-toggle="modal"
                                       data-target="#updateBill<?php echo $creditInfo['id']; ?>">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                             stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-edit-3">
                                            <path d="M12 20h9"></path>
                                            <path
                                                d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path>
                                        </svg>
                                    </a>
                                </td>
                            </tr>
                            <div class="modal fade" id="updateBill<?php echo $creditInfo['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">জমার  তথ্য পরিবর্তন  </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="widget-one">
                                                <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                                                    <div class="form-group mb-4">
                                                        <label for="billDesc">ব্যক্তি নাম </label>
                                                        <select name="person_id" id="person_id" class="form-control">
                                                            <?php
                                                                $person_name = getAllPersong();
                                                                while ($personInfo = mysqli_fetch_assoc($person_name)){ ?>
                                                                    <option value="<?php echo $personInfo['id'];?>" <?php if($personInfo['id'] == $creditInfo['person_id']){ echo 'selected';}?>><?php echo $personInfo['person_name'];?></option>
                                                                <?php } ?>
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group mb-4">
                                                        <label for="credit_date">তারিক </label>
                                                        <input id="credit_date" type="date" name="credit_date" value="<?php echo $creditInfo['credit_date']; ?>"
                                                               class="form-control" required="">
                                                    </div>
                                                    <div class="form-group mb-4">
                                                        <label for="credit_date">টাকার পরিমান </label>
                                                        <input id="credit_amount" type="text" name="credit_amount" value="<?php echo $creditInfo['credit_amount']; ?>"
                                                               class="form-control" required="">
                                                    </div>
                                                    <div class="form-group mb-4">
                                                        <label for="reference">রেফারেন্সেস </label>
                                                        <input id="reference" type="text" name="reference" value="<?php echo $creditInfo['reference']; ?>"
                                                               class="form-control" required="">
                                                    </div>
                                                    <input type="submit" name="submit" value="সাবমিট" class="btn btn-primary btn-block mb-4 mr-2">
                                                    <input type="hidden" name="id"  value="<?php echo $creditInfo['id']; ?>">
                                                    <input type="hidden" name="action" id="action" value="updateBill">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <?php $count++;
                } ?>
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- End Content -->
<?php include "footer.php"; ?>

<script>
    function confirmDelete() {
        if (confirm("Are you sure want to delete?")) {
            return true;
        }
        return false;
    }

    $(document).ready(function(){
        var maxField = 5;
        var addButton = $('.add_button');
        var wrapper = $('.field_wrapper');
        var fieldHTML = '<div>' +
            '<a href="javascript:void(0);" class="remove_button btn btn-danger mt-2 mb-4  btn-sm">' +
            '<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><line x1="8" y1="12" x2="16" y2="12"></line></svg>' +
            '</a>' +
            '<input type="text" name="billDesc[]" placeholder="Description here" class="form-control"/>' +' <input type="text" class="form-control mt-2" name="billAmount[]" placeholder="0.00"  required/>'+
            '</div>';
        var x = 1;

        $(addButton).click(function(){
            if(x < maxField){
                x++;
                $(wrapper).append(fieldHTML);
            }
        });

        $(wrapper).on('click', '.remove_button', function(e){
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
        });
    });
</script>
